# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Corpus Module to provide helper functions for the other modules of textspy.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-01-21
Version : 1.0: 2022-11-11

"""

from colorama import *
from datetime import datetime
from gensim.corpora.dictionary import Dictionary as gensimDict
from loguru import logger
from loguru._logger import Logger
import matplotlib.figure as figure
import matplotlib.pyplot as plt
from numpy import float64
from numpy import ndarray
import os
import pandas as pd
from pandas.core.frame import DataFrame as pandasDF
import pyLDAvis
from spacy.language import Language as spacyNLP
from spacy.tokens.doc import Doc as spacyDoc
from spacy.tokens.span import Span as spacySpan
from spacy.tokens.token import Token as spacyToken
import string
from typing import Iterable, Literal, TypedDict
from wordcloud import WordCloud

from . import ldamallet

if not spacyDoc.has_extension("metadata"):
    spacyDoc.set_extension("metadata", default=None)

logger.remove()

"""****************************************************
    Helpers TypedDict Classes
    ~~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

class CorpusFrequencyWeightDict(TypedDict):
    frequency: dict[str, int]
    weight: dict[str, float]

class DataDict(TypedDict):
    data: ndarray | Iterable | dict | pandasDF
    title: str

class DataFrameDict(TypedDict):
    df: pandasDF
    df_title: str
    index: bool

class DataFramesListDict(TypedDict):
    dataframes: list[DataFrameDict]
    title: str

class RequestedDataMetadataDict(TypedDict):
    request_date: str
    request_url: str
    type: str

class VersionsMetadataDict(TypedDict):
    title: str

class VersionsDict(TypedDict):
    data: str
    metadata: VersionsMetadataDict

class WorksMetadataDict(TypedDict):
    title: str
    year: str

class WorksDict(TypedDict):
    metadata: WorksMetadataDict
    versions: list[VersionsDict]

class RequestedDataDict(TypedDict):
    metadata: RequestedDataMetadataDict
    works: list[WorksDict]

class FigureDict(TypedDict):
    title: str
    figure: figure.Figure

class FrequenciesDict(TypedDict):
    data: dict[str, float]
    title: str

class FrequencyWeightDict(TypedDict):
    frequency: int
    weight: float

# class modeDict(TypedDict):
#     mode: str
#     min_docs: NotRequired[int]
#     max_docs: NotRequired[int]
#     stopwords_file : NotRequired[str]
# Funktioniert erst ab Python 3.11 https://peps.python.org/pep-0655/

class ModelDict(TypedDict):
    CoherenceScore: float64
    Model: ldamallet.LdaMallet
    NumberOfTopics: int

class ModelIndexDict(TypedDict):
    CoherenceScore: float64
    Index: int
    Model: ldamallet.LdaMallet
    NumberOfTopics: int

class QuantitiesDict(TypedDict):
    characters: int
    words: int
    word_tokens: int

class CorpusVersionsDict(TypedDict):
    data: str
    metadata: VersionsMetadataDict
    quantities: QuantitiesDict
    spaCy_Doc: spacyDoc

class CorpusWorksDict(TypedDict):
    metadata: WorksMetadataDict
    versions: list[CorpusVersionsDict]

class CorpusDict(TypedDict):
    metadata: RequestedDataMetadataDict
    works: list[CorpusWorksDict]

class DependenciesDict(TypedDict):
    dependencyFrequency: dict[str, int]
    dependencyType: str | list[str]
    sentenceNumber: int
    title: str

class FilteredCorpusVersionsDict(TypedDict):
    data: str
    filtered: list[spacyToken]
    metadata: VersionsMetadataDict
    quantities: QuantitiesDict
    spaCy_Doc: spacyDoc

class FilteredCorpusWorksDict(TypedDict):
    metadata: WorksMetadataDict
    versions: list[FilteredCorpusVersionsDict]

class FilteredCorpusDict(TypedDict):
    metadata: RequestedDataMetadataDict
    works: list[FilteredCorpusWorksDict]

class CorpusFrequenciesVersionsDict(TypedDict):
    data: str
    filtered: list[spacyToken]
    metadata: VersionsMetadataDict
    quantities: QuantitiesDict
    spaCy_Doc: spacyDoc
    Frequencies : FrequenciesDict
    Weights: FrequenciesDict

class CorpusFrequenciesWorksDict(TypedDict):
    metadata: WorksMetadataDict
    versions: list[CorpusFrequenciesVersionsDict]

class CorpusFrequenciesDict(TypedDict):
    metadata: RequestedDataMetadataDict
    works: list[CorpusFrequenciesWorksDict]

class PatternDict(TypedDict):
    title: str
    patterns: list[list[dict[str, str | dict]]]

class RetokenizerTermsDict(TypedDict):
    terms: tuple[str, ...]
    lemma: str

class SpacyDocDict(TypedDict):
    spaCy_Doc: spacyDoc
    title: str
    word_tokens: int

class TitlesBagOfWordsDict(TypedDict):
    BagOfWords: list[str]
    title: str

class ForTopicModelingDict(TypedDict):
    GensimDictionary: gensimDict
    TermDocumentFrequency: list[list[tuple[int, int]]]
    TitlesBagOfWords: list[TitlesBagOfWordsDict]

class TitleDict(TypedDict):
    TextTitle: str
    VersionTitle: str

# class TokenDict(TypedDict):
#     spaCyToken: spacyToken
#     posTags: NotRequired[list[str]]
# Funktioniert erst ab Python 3.11 https://peps.python.org/pep-0655/

class TokenDict(TypedDict):
    spaCy_Token: spacyToken

class TokenPosDict(TokenDict):
    posTags: list[str]

class TopicTitleIDDict(TypedDict):
    Title: str
    id: str

class TokenInstancesDict(TypedDict):
    Title: str
    Data: dict[str, list[spacyToken]]

class TokenTopicWeightsMetadtaDict(TypedDict):
    NumberOfTopics: int
    Topics: list[TopicTitleIDDict]

class TokenTopicWeightsDict(TypedDict):
    Metadata: TokenTopicWeightsMetadtaDict
    Tokens: list[dict]

class TokenDifferenceDict(TypedDict):
    Token: str
    Weight_Difference: float64

class ComparisonDict(TypedDict):
    Topics: tuple[str, str]
    Difference: list[TokenDifferenceDict]
    HighestDifference: DataFrameDict
    LowestDifference: DataFrameDict

class TopicsDocumentsDict(TypedDict):
    dataframes: list[DataFrameDict]
    title: str

class WordContextDict(TypedDict):
    Data: dict[str, list[spacySpan]]
    Title: str
    Type: Literal['context paragraph', 'context sentence', 'context span']

class WordCorpusContextDict(TypedDict):
    Data: dict[str, dict[str, list[spacySpan]]]
    Title: str
    Type: Literal['context paragraph', 'context sentence', 'context span']

class WordFrequenciesDict(TypedDict):
    Data: dict[str, dict[str, int]]
    Title: str

class WordCooccurrencesDict(WordFrequenciesDict):
    Type: Literal['cooccurrences document', 'cooccurrences paragraph', 'cooccurrences sentence', 'cooccurrences span']

class WordDependenciesDict(WordFrequenciesDict):
    Type: Literal['dependencies custom', 'dependencies subordinate', 'dependencies superior']

class WordCorpusFrequenciesDict(TypedDict):
    Data: dict[str, dict[str, dict[str, int]]]
    Title: str

class WordCorpusCooccurrencesDict(WordCorpusFrequenciesDict):
    Type: Literal['cooccurrences document', 'cooccurrences paragraph', 'cooccurrences sentence', 'cooccurrences span']

class WordCorpusDependenciesDict(WordCorpusFrequenciesDict):
    Type: Literal['dependencies custom', 'dependencies subordinate', 'dependencies superior']

class WordCorpusFrequencyWeightDict(TypedDict):
    Data: dict[str, CorpusFrequencyWeightDict]
    Title: str
    Type: Literal['frequency weight']

class WordFrequencyWeightDict(TypedDict):
    Data: dict[str, FrequencyWeightDict]
    Title: str
    Type: Literal['frequency weight']

"""****************************************************
    Helpers Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def create_filename(name: str, length: int = 50, log_dir: str ='logging/') -> str:
    """Converting any string into a string that fits for a filename.

    Args:
        name (str) : any string
        length (int) : maximum length of filename
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        filename (str) : string that fits for a filename
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    special_char_map = {ord('ä'):'ae', ord('ü'):'ue', ord('ö'):'oe', ord('ß'):'ss'}
    filename = name.lower()
    filename = filename.translate(special_char_map)
    filename = filename.translate(str.maketrans('', '', string.punctuation))
    filename = " ".join(filename.split())
    filename = filename.replace(" ", "-")
    if len(filename) > length:
        filename = filename[0:length-20] + '_' + filename[-10:]
    logger.info('function finished')
    return filename

def get_all_bag_of_words(titles_filtered: list[TitlesBagOfWordsDict], log_dir: str ='logging/') -> list[list[str]]:
    """Returning all bag of words in a list.

    Args:
        titles_filtered (list) : List which consists of dictionaries containing titles and bag of words
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        bag_of_words_list (list) : List which consists of all bag of words
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    bag_of_words_list = []
    for text in titles_filtered:
        bag_of_words_list.append(text['BagOfWords'])
    logger.info('function finished')
    return bag_of_words_list

# def get_list_of_values(list_of_dictionaries: list[dict], log_dir: str ='logging/') -> list:
#     """Getting the first value of all dictionaries stored in a list.

#     Args:
#         list_of_dictionaries (list) : List which consists of dictionaries
#         log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

#     Returns:
#         list_of_values (list) : List which consists of the first values of the items of list_of_dictionaries
#     """
#     logger = logging_init(log_dir)
#     logger.info('function started')
#     list_of_values = []
#     for itm in list_of_dictionaries:
#         list_of_values.append(list(itm.values())[0])
#     logger.info('function finished')
#     return list_of_values

def get_paragraphs(spacy_Doc: spacyDoc, log_dir: str ='logging/') -> list[spacySpan]:
    """Returns all paragraphs of the document as a list.

    Args:
        spacy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        paragraph_list (list) : List of https://spacy.io/api/span
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    paragraph_list = []
    index_list = [token.i for token in spacy_Doc if token._.is_paragraph_ending]
    last_index = len(spacy_Doc)-1
    index_list.extend([0, last_index])
    index_list = list(dict.fromkeys(index_list))
    index_list.sort()
    i = 0
    for index in index_list:
        try:
            index_list[i+1]
        except:
            pass
        else:
            start_index = index+1
            end_index = index_list[i+1]
            paragraph = spacy_Doc[start_index:end_index]
            paragraph_list.append(paragraph)
        i += 1
    logger.info('function finished')
    return paragraph_list

def image_to_figure(image,
                    title: str,
                    img_width: int = 100,
                    img_height: int = 100,
                    axis: bool | str = False,
                    log_dir: str ='logging/',
                    **titlekwargs) -> FigureDict:
    """Plotting a word cloud as matplotlib.figure.Figure.

    Args:
        wordcloud : array-like or PIL image (siehe https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.imshow.html)
        title (str) : Title of the Figure
        img_width (int) : Width of the Figure
        img_height (int) : Height of the Figure
        axis (bool | str): Option-parameter for matplotlib.pyplot.axis (https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.axis.html)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **titlekwargs () : This parameter allows to use optional keyword-arguments for matplotlib.pyplot.title (https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.title.html)

    Returns:
        wordcloud_dict (dict) : Dictionaries containing 'figure' (matplotlib.figure.Figure) and 'title' (str)
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    plt.figure(figsize=[img_width, img_height])
    plt.imshow(image)
    plt.axis(axis)
    plt.title(title, **titlekwargs)
    wordcloud_figure = plt.gcf()
    wordcloud_dict = {
        'title' : title,
        'figure' : wordcloud_figure
    }
    logger.info('function finished')
    return wordcloud_dict

def lemmas_from_tokens(token_list: list[spacyToken], lowercase: bool = False, log_dir: str ='logging/') -> list[str]:
    """Creates a list of lemmatized words from a list of spaCy tokens.

    Args:
        token_list (list) : List of spacy.tokens.token.Token
        lowercase (boolean) : If true, lemmatized words will normalized
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        lemmas (list) : List of lemmatized words
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    lemmas = []
    for token in token_list:
        if lowercase==False:
            lemmas.append(token.lemma_)
        else:
            lemmas.append(token.lemma_.lower())
    logger.info('function finished')
    return lemmas

def logging_init(log_dir: str ='logging/') -> Logger:
    """Initializes new logger if no logger already exists.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        logger (loguru._logger.Logger) : Loguru logger
    """
    if len(logger._core.handlers) >= 1:
        # see https://github.com/Delgan/loguru/issues/201#issuecomment-575333343
        logger.info('Logger already exists')
        return logger
    else:
        try:
            os.makedirs(log_dir)
        except:
            pass
        logger.add(log_dir + datetime.today().strftime('%Y-%m-%d-%H-%M-%S') + '.log')
        logger.info('initialized new logger')
    return logger

def make_dataframe_dict(data_dict: DataDict, hasindex=True, log_dir: str ='logging/', **DataFrameKWARGS) -> DataFrameDict:
    """Creates dictionary wich contains a DataFrame.

    Args:
        data_dict (dict) : Dictionary with the following keys: 'title' and 'data' (ndarray (structured or homogeneous), Iterable, dict, or DataFrame)
        hasindex (boolean) : If index should be included or not
        **DataFrameKWARGS : Keyword arguments for pandas.DataFrame (see https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        dataframe_dict (dict) : dictionary which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not)
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    df = pd.DataFrame(data_dict['data'], **DataFrameKWARGS)
    dataframe_dict = {
        'df' : df,
        'index' : hasindex,
        'df_title' : data_dict['title']
    }
    logger.info('function finished')
    return dataframe_dict

def make_spacy_docs(nlp: spacyNLP,
                   text_tuple_list: list[tuple[str, dict]],
                   log_dir: str ='logging/',
                   **pipekwargs
                   ) -> list[spacyDoc]:
    """Creates a List of spaCy document objects.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        text_tuple_list (list) : List with tuples which consist of two elements: first text as string, second metadata as dictionary (must include the keys 'title' and 'version')
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **pipekwargs () : This parameter allows to use optional keyword-arguments for spacyNLP.pipe (https://spacy.io/api/language#pipe)

    Returns:
        spaCy_Doc_list (list) : List of spacy.tokens.doc.Doc
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    spaCy_Doc_list = []
    for spaCy_Doc, metadata in nlp.pipe(text_tuple_list, as_tuples=True, **pipekwargs):
        spaCy_Doc._.metadata = metadata
        spaCy_Doc_list.append(spaCy_Doc)
    logger.info('function finished')
    return spaCy_Doc_list

def remove_spacy_duplicates(spacy_token_list: list[spacyToken], log_dir: str ='logging/') -> list[spacyToken]:
    """Removes any duplicates from a list consisting of spacy.tokens.token.Token.

    Args:
        spacy_token_list (list) : List consisting of spacy.tokens.token.Token with duplicate lemmas
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        (list) : List consisting of spacy.tokens.token.Token without duplicate lemmas
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    uniques_dict = {}
    for item in spacy_token_list:
        uniques_dict.update({item.lemma : item})
    uniques_list = []
    for dict_obj in uniques_dict:
        uniques_list.append(uniques_dict[dict_obj])
    logger.info('function finished')
    return uniques_list

def save_DataFrameDict_csv(title_workbook: str,
                           data_frame_dict: DataFrameDict,
                           output_dir: str,
                           na_rep: str='0',
                           log_dir: str ='logging/',
                           **tocsvkwargs
                           ) -> None:
    """Saving a DataFrame as a comma-separated values (csv) file to a specified directory.

    Args:
        title_workbook (str) : Title of the excel workbook
        data_frame_dict (dict) : Dictionary which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not)
        output_dir (str) : directory where output will be saved
        na_rep (str) = Representation of NaN-Values
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : data frame as csv file
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(title_workbook + ' ' + data_frame_dict['df_title'])
    try:
        os.makedirs(output_dir)
    except:
        pass
    data_frame_dict['df'].to_csv(output_dir + '/' + filename +'.csv', na_rep=na_rep, index=data_frame_dict['index'], **tocsvkwargs)
    logger.info('function finished')

def save_DataFrameDict_excel(title_workbook: str,
                             data_frame_dict: DataFrameDict,
                             output_dir: str,
                             na_rep: str='0',
                             log_dir: str ='logging/',
                             **toexcelkwargs
                             ) -> None:
    """Saving a DataFrame as an excel file to a specified directory.

    Args:
        title_workbook (str) : Title of the excel workbook
        data_frame_dict (dict) : Dictionary which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not)
        output_dir (str) : directory where output will be saved
        na_rep (str) = Representation of NaN-Values
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : data frame as excel file
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(title_workbook)
    try:
        os.makedirs(output_dir)
    except:
        pass
    sheet_name = create_filename(data_frame_dict['df_title'], 30)
    data_frame_dict['df'].to_excel(output_dir + '/' + filename +'.xlsx', sheet_name=sheet_name, na_rep=na_rep, index=data_frame_dict['index'], **toexcelkwargs)
    logger.info('function finished')

def save_DataFramesListDict_csv(data_frame_list_dict: DataFramesListDict,
                                output_dir: str,
                                na_rep: str='0',
                                log_dir: str ='logging/',
                                **tocsvkwargs
                                ) -> None:
    """Saving DataFrames as a comma-separated values (csv) file to a specified directory.

    Args:
        data_frame_list_dict (dict) : Dictionary containing 'title' (str) and 'dataframes' (list of dictionaries which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not))
        output_dir (str) : directory where output will be saved
        na_rep (str) = Representation of NaN-Values
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves one excel file with DataFrames as sheets
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(data_frame_list_dict['title'])
    try:
        os.makedirs(output_dir)
    except:
        pass
    for DataFrame in data_frame_list_dict['dataframes']:
        sheet_name = create_filename(filename + ' ' + DataFrame['df_title'])
        DataFrame['df'].to_csv(output_dir + '/' + sheet_name + '.csv', na_rep=na_rep, index=DataFrame['index'], **tocsvkwargs)
    logger.info('function finished')

def save_DataFramesListDict_excel(data_frame_list_dict: DataFramesListDict,
                                 output_dir: str,
                                 na_rep: str='0',
                                 log_dir: str ='logging/',
                                 **excelwriterkwargs
                                 ) -> None:
    """Saving DataFrames as an excel file to a specified directory.

    Args:
        data_frame_list_dict (dict) : Dictionary containing 'title' (str) and 'dataframes' (list of dictionaries which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not))
        output_dir (str) : directory where output will be saved
        na_rep (str) = Representation of NaN-Values
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves one excel file with DataFrames as sheets
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(data_frame_list_dict['title'])
    try:
        os.makedirs(output_dir)
    except:
        pass
    with pd.ExcelWriter(output_dir + '/' + filename +'.xlsx', **excelwriterkwargs) as writer:
        for DataFrame in data_frame_list_dict['dataframes']:
            sheet_name = create_filename(DataFrame['df_title'], 30)
            DataFrame['df'].to_excel(writer, sheet_name=sheet_name, na_rep=na_rep, index=DataFrame['index'])
    logger.info('function finished')

def save_Figure_list(figure_list: list[FigureDict],
                     output_dir: str,
                     pdf: bool = True,
                     svg: bool = True,
                     log_dir: str ='logging/'
                     ) -> None:
    """Saving a list of Figures as PDF and/or SVG files to a specified directory.

    Args:
        figure_list (list) : List of dictionaries which contain the keys 'title' (str) and 'figure' (figure.Figure)
        output_dir (str) : directory where output will be saved
        pdf (boolean) : If true, figures will be saved as PDF-Files
        svg (boolean) : If true, figures will be saved as SVG-Files
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves PDF- and/or SVG-files
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    for figure in figure_list:
        if pdf == True:
            save_Figure_to_pdf(figure['title'], output_dir, figure['figure'],)
            if svg == True:
                save_Figure_to_svg(figure['title'], output_dir, figure['figure'],)
        elif svg == True:
                save_Figure_to_svg(figure['title'], output_dir, figure['figure'],)
        else:
            print(Fore.RED + 'Bitte setze svg und/oder pdf auf True um Figures zu speichern.' + Style.RESET_ALL)
    logger.info('function finished')

def save_Figure_to_pdf(title_figure: str,
                       output_dir: str,
                       figure: figure,
                       log_dir: str ='logging/'
                       ) -> None:
    """Saving a Figure as a PDF file to a specified directory.

    Args:
        title_figure (str) : Title of the figure
        output_dir (str) : directory where output will be saved
        figure (matplotlib.figure.Figure) : Figure
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves one PDF file
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(title_figure)
    try:
        os.makedirs(output_dir)
    except:
        pass
    figure.savefig(output_dir + '/' + filename +'.pdf', format='pdf')
    plt.close(figure)
    logger.info('function finished')

def save_Figure_to_svg(title_figure: str,
                       output_dir: str,
                       figure: figure,
                       log_dir: str ='logging/'
                       ) -> None:
    """Saving a Figure as a SVG file to a specified directory.

    Args:
        title_figure (str) : Title of the figure
        output_dir (str) : directory where output will be saved
        figure (matplotlib.figure.Figure) : Figure
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves one SVG file
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(title_figure)
    try:
        os.makedirs(output_dir)
    except:
        pass
    figure.savefig(output_dir + '/' + filename +'.svg', format='svg')
    plt.close(figure)
    logger.info('function finished')

def save_topic_model_to_html(title_topic_model: str,
                             output_dir: str,
                             topic_model: pyLDAvis._prepare.PreparedData,
                             log_dir: str ='logging/'
                             ) -> None:
    """Saving a Topic Model as a HTML file to a specified directory.

    Args:
        title_topic_model (str) : Title of the figure
        output_dir (str) : directory where output will be saved
        topic_model (pyLDAvis._prepare.PreparedData) : Visualized topic model
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        None : saves one HTML file
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    filename = create_filename(title_topic_model)
    try:
        os.makedirs(output_dir)
    except:
        pass
    pyLDAvis.save_html(topic_model, output_dir + '/' + filename +'.html')
    logger.info('function finished')

def sort_frequencies(frequencies: dict[str, float], log_dir: str ='logging/') -> dict[str, float]:
    """Sorting a frequency dict.

    Args:
        frequencies (dict) : Words and frequencies as a dictionary
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        sorted_frequencies (dict) : Words and frequencies as a dictionary sortedy by descending frequencies
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    sorted_frequencies = dict(sorted(frequencies.items(), key=lambda item: item[1], reverse=True))
    logger.info('function finished')
    return sorted_frequencies

def user_input_float(input_text: str, log_dir: str ='logging/') -> float | None:
    """Asking the user for a float. If the input cannot be converted to a float, this functions will return None. If it can be converted, the input will be returned as a float.

    Args:
        input_text (str) : Text for communication with user
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        input_float (int) : Float
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    input_float_string = input(input_text)
    try:
        input_float = float(input_float_string)
        logger.info('function finished')
        return input_float
    except:
        logger.warning('Could not convert user input to float. Function returns None.')
        return None

def user_input_integer(input_text: str, log_dir: str ='logging/') -> int | None:
    """Asking the user for an integer. If the input is not an integer, this functions returns None. If it can be converted, the input will be returned as an integer.

    Args:
        input_text (str) : Text for communication with user
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        input_integer (int) : Integer
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    input_integer_string = input(input_text)
    try:
        input_integer = int(input_integer_string)
        logger.info('function finished')
        return input_integer
    except:
        logger.warning('Could not convert user input to integer. Function returns None.')
        return None

def user_input_specific_word(nlp: spacyNLP, log_dir: str ='logging/') -> None | spacyToken:
    """Asking the user for a word and returning it as a spaCy token.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        token_word (spacy.tokens.token.Token) : https://spacy.io/api/token
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    input_word = input('Bitte geben Sie ein Wort ein: ')
    if not input_word:
        logger.info('function finished (returns None)')
        return None
    else:
        token_word = nlp(input_word)[0]
        if len(token_word) != len(input_word):
            logger.info('function finished (returns None)')
            return None
        else:
            logger.info('function finished (returns token)')
            return token_word

def user_input_specific_POS(log_dir: str ='logging/') -> list[str | None]:
    """Asking the user for POS-tags and returning them as a list.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        POS_tags (list) : List of POS-tags
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    POS_tags = input('Optional: Bitte geben Sie einen oder mehrere (durch Leerzeichen getrennte) POS-Tags ein: ').split()
    if not POS_tags:
        logger.info('function finished (returns None)')
        return None
    else:
        POS_tags_upper = [x.upper() for x in POS_tags]
        logger.info('function finished (returns POS-tag)')
        return POS_tags_upper

def user_input_terms(log_dir: str ='logging/') -> None | tuple[str]:
    """Asking the user for terms (as one string) and returns them as a tuple.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        terms_tuple (None | tuple) : None or tuple consisting of lemmas (str)
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    input_terms = input('Bitte geben Sie die Terme ein. Die einzelnen Terme müssen ausschließlich durch Leerzeichen separiert sein.')
    if not input_terms:
        logger.info('function finished (returns None)')
        return None
    else:
        terms_tuple = tuple(input_terms.split())
        logger.info('function finished (returns terms)')
        return terms_tuple

def user_input_terms_lemma_loop(log_dir: str ='logging/') -> list[RetokenizerTermsDict]:
    """Creating a list of terms and related lemma from user input. The user will be asked for more terms and lemma as long as he/she does not enter no term.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        term_lemma_dict_list (list) : List consisting of dictionaries with keys 'terms' (value: tuple with components of ngram in right order) and 'lemma' (value: lemma of ngram as string)
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    term_lemma_dict_list = []
    while True:
        input_terms = user_input_terms(log_dir)
        if input_terms == None:
            print(Fore.RED + 'Eingabe beendet' + Style.RESET_ALL)
            break
        else:
            input_lemma = input('Bitte geben Sie die lemmatisierte Form der Phrase ein')
            if not input_lemma:
                print(Fore.RED + 'Kein Lemma eingegeben. Starten Sie die Eingabe mit neuen Terme neu.' + Style.RESET_ALL)
                continue
            else:
                input_terms_lemma = {
                    'terms' : input_terms,
                    'lemma' : input_lemma
                }
                term_lemma_dict_list.append(input_terms_lemma)
    logger.info('function finished')
    return term_lemma_dict_list

def user_input_tokens_POS(list_word_pos: list[TokenDict | TokenPosDict],
                          input_text: str,
                          nlp: spacyNLP,
                          log_dir: str ='logging/'
                          ) -> list[TokenDict | TokenPosDict] | list:
    """Lets the user choose between a pre-configured list of tokens (optionally: and related POS-tags) or to create a new list out of user input.

    Args:
        list_word_pos (list) : List of dicitionaries with spacy.tokens.token.Token and optionally related POS-tags
        input_text (str) : Text for user-communication
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        token_dict_list (list) : List of dicitionaries with spacy.tokens.token.Token and optionally related POS-tags
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    all_tokens_from_string = input(input_text)
    if all_tokens_from_string == 'Ja':
        print(Fore.BLUE + 'Verwendung der gegebenen Token-Liste' + Style.RESET_ALL)
        token_dict_list = list_word_pos
    else:
        print(Fore.BLUE + 'Geben Sie neue Wörter (und ggf. POS-Tags) ein' + Style.RESET_ALL)
        token_dict_list = user_input_word_POS_loop(nlp)
    logger.info('function finished')
    return token_dict_list


def user_input_word_POS_loop(nlp: spacyNLP, log_dir: str ='logging/') -> list[TokenDict | TokenPosDict]:
    """Creating a list of word-tokens and related POS-tags from user input. The user will be asked for more words and POS-tags as long as he/she does not enter no word.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        token_dict_list (list) : List of dicitionaries with spacy.tokens.token.Token and optionally related POS-tags
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    token_dict_list = []
    while True:
        input_word = user_input_specific_word(nlp)
        if input_word == None:
            print(Fore.RED + 'Eingabe beendet' + Style.RESET_ALL)
            break
        else:
            input_word_pos = {
                'spaCy_Token' : input_word
            }
            input_pos = user_input_specific_POS()
            if input_pos != None:
                input_word_pos.update({'POS_tags' : input_pos})
            token_dict_list.append(input_word_pos)
    logger.info('function finished')
    return token_dict_list

def wordcloud_filter_on_percentage(frequencies: dict[str, float],
                                   percentage: float | None = None,
                                   log_dir: str ='logging/'
                                   ) -> dict[str, float]:
    """Filters a frequency dictionary by certain percentage

    Args:
        frequencies (dict) : Words and frequencies as a dictionary
        percentage (float | None) : Percentage as integer; if None default (= 0.1)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        filtered_frequencies (dict) : Filtered frequencies
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    if percentage == None:
        percentage = 0.1
    total_number = 0
    for x in frequencies.values():
        total_number = total_number + x
    p = percentage * 0.01 * total_number
    filtered_frequencies = {}
    for y, z in frequencies.items():
        if z >= p:
            filtered_frequencies.update({y : z})
    logger.info('function finished')
    return(filtered_frequencies)

def wordcloud_from_frequency(frequencies: dict[str, float],
                             max_words: int = 200,
                             log_dir: str ='logging/'
                             ) -> WordCloud:
    """Creating a wordcloud of the most frequent words on basis of a frequency dictionary.

    Args:
        frequencies (dict) : Words and frequencies as a dictionary
        max_words (int) : Maximum number of words displayed in the wordcloud
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        wordcloud : class Word cloud object
    """
    logger = logging_init(log_dir)
    logger.info('function started')
    wordcloud = WordCloud(background_color='white',
                        width=5000,
                        height=3600,
                        max_words=max_words,
                        colormap='tab10').generate_from_frequencies(frequencies)
    logger.info('function finished')
    return wordcloud

def wordcloud_pipeline(frequencies_dict: FrequenciesDict,
                       logger: Logger,
                       max_words: int | None = None,
                       percentage: float | None = None,
                       img_width: int = 100,
                       img_height: int = 100,
                       fontsize: float = 96.0,
                       log_dir: str ='logging/'
                       ) -> FigureDict:
    """Pipeline for creating, displaying, and saving of wordclouds based on frequencies.

    Args:
        frequencies_dict (dict) : Dictionary containing 'data' (dict[str, float]) and 'title' (str)
        logger (Logger) : Preconfigured Loguru-Logger (it is necessary to use this preconfigured
        max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
        percentage (float | None) : Percentage as integer; if None default (= 0.1)
        img_width (int) : Width of the word cloud
        img_height (int) : Height of the word cloud
        fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        wordcloud : class Word cloud object
    """
    logger.info('function started')
    if max_words == None:
        logger.info('Maximum number of words will be calculated from percentage')
        reduced_frequencies_dict = wordcloud_filter_on_percentage(frequencies=frequencies_dict['data'], percentage=percentage, log_dir=log_dir)
        max_words = len(reduced_frequencies_dict)
    else:
        logger.info('Given maximum number of words will be used')
    wordcloud = wordcloud_from_frequency(frequencies_dict['data'], max_words, log_dir=log_dir)
    wordcloud_dict = image_to_figure(image=wordcloud, title=frequencies_dict['title'], img_width=img_width, img_height=img_height, log_dir=log_dir, fontsize=fontsize)
    logger.info('function finished')
    return wordcloud_dict