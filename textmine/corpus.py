# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Module to preprocess a corpus for topic modeling and other forms of analyses.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-02-17
Version : 1.0: 2022-10-17

"""

import copy
import gensim
import pandas as pd
import re
from spacy.language import Language as spacyNLP
from spacy.tokens.doc import Doc as spacyDoc
from spacy.tokens.token import Token as spacyToken

from . import helpers

"""****************************************************
    Corpus Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def count_words(text: str, log_dir: str ='logging/') -> int:
    """Counting the words.

    Args:
        text (str)    : Text as string
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        words_number (int) : Number of words
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    words_number = len(re.findall(r'\w+', text))
    logger.info('function finished')
    return words_number

def count_word_tokens(spacy_Doc: spacyDoc, log_dir: str ='logging/') -> int:
    """Counting the word tokens.

    Args:
        spacy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        word_tokens_number (int) : Number of word tokens
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    word_tokens_number = 0
    for token in spacy_Doc:
        if not token.pos_ in ['PUNCT', 'SPACE', 'SYM', 'X']:
            word_tokens_number += 1
    logger.info('function finished')
    return word_tokens_number

def create_stopset(stopwords_file: str, log_dir: str ='logging/') -> str:
    """Creating a set of stop words.

    Args:
        stopwords_file (str)    : Path of the file with stop words

    Returns:
        stopset (set) : Set of stop words
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    with open(stopwords_file, encoding="utf8") as f:
        stoptext = f.read()
    stopset = set(stoptext.split())
    logger.info('function finished')
    return stopset

def filter_corpus(works: list[helpers.CorpusWorksDict],
                   mode: dict | None = None,
                   allowed_postags: list | None = None,
                   log_dir: str ='logging/'
                   ) -> list[helpers.FilteredCorpusWorksDict]:
        """Adds a filtered list of spaCy tokens to each text version in a corpus.

        Args:
            works (list) : list of dictionaries with spaCy-Doc and quantities (characters, words, and word tokens) for every text-version
            mode (dict | None) : Dictionary which configures the mode with the keys 'mode' (required, value: 'pruning' or 'stopwords'), 'min_docs' (optional, value: int), 'max_docs' (optional, value: int), 'stopwords_file' (optional, value: str); or None instead of this dict will
            allowed_postags (list | None): List of https://spacy.io/api/annotation or None

        Returns:
            filteredCorpus (list) : Corpus updated with a filtered list of spacy.tokens.token.Token for every text-version
        """
        logger = helpers.logging_init(log_dir)
        logger.info('function started')
        if allowed_postags == None:
            logger.info('default POS-tags')
            allowed_postags = ['NOUN', 'ADJ', 'VERB', 'ADV']
        filteredCorpus = copy.deepcopy(works)
        if mode == None:
            logger.info('filters corpus with pruning')
            filter_mode = 'pruning'
            min_docs = 10
            max_docs = 90
            filtered_tokens = make_filtered_tokens(filteredCorpus, min_docs, max_docs, allowed_postags)
        elif ('mode', 'pruning') in mode.items():
            logger.info('filters corpus with pruning')
            filter_mode = 'pruning'
            if 'min_docs' in mode:
                min_docs = mode['min_docs']
            else:
                min_docs = 10
            if 'max_docs' in mode:
                max_docs = mode['max_docs']
            else:
                max_docs = 90
            filtered_tokens = make_filtered_tokens(filteredCorpus, min_docs, max_docs, allowed_postags)
        elif ('mode', 'stopwords') in mode.items():
            logger.info('filters corpus with stop words')
            filter_mode = 'stopwords'
            stopset = create_stopset(mode['stopwords_file'])
        logger.info('Corpus filtering started')
        for text in filteredCorpus:
            for version in text['versions']:
                if filter_mode == 'pruning':
                    logger.info('filters ' + text['metadata']['title'] + ' ' + version['metadata']['title'] + ' with pruning')
                    filtered = make_filtered_pruning(version['spaCy_Doc'], filtered_tokens)
                elif filter_mode == 'stopwords':
                    logger.info('filters ' + text['metadata']['title'] + ' ' + version['metadata']['title'] + ' with stop words')
                    filtered = make_filtered(version['spaCy_Doc'], stopset, allowed_postags)
                version.update({'filtered' : filtered})
        logger.info('function finished')
        return filteredCorpus

def make_filtered(spaCy_Doc,
                  stopset: set,
                  allowed_postags: list = ['NOUN', 'ADJ', 'VERB', 'ADV'],
                  log_dir: str ='logging/'
                  ) -> list[spacyToken]:
    """Creating a filtered list of spaCy tokens (only certain POS and reduced from stopwords).

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        stopset (set) : Set of stop words
        allowed_postags (list) : List of POS-tags
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        filtered (list) : Filtered list of spacy.tokens.token.Token
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    filtered = []
    for token in spaCy_Doc:
        if token.pos_ in allowed_postags:
            filtered.append(token)
    filtered = [w for w in filtered if not w.lemma_.lower() in stopset]
    logger.info('function finished')
    return filtered

def make_filtered_pruning(spaCy_Doc,
                          filtered_tokens: list,
                          log_dir: str ='logging/'
                          ) -> list[spacyToken]:
    """Creating a filtered list of spaCy tokens (based on list of eligible spaCy tokens).

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        filtered_tokens (list) : List of string (spacy.tokens.token.Token.lemma_)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        filtered (list) : Filtered list of spacy.tokens.token.Token
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    filtered = []
    for token in spaCy_Doc:
        if token.lemma_ in filtered_tokens:
            filtered.append(token)
    logger.info('function finished')
    return filtered

def make_filtered_tokens(corpus: helpers.CorpusWorksDict,
                         min_rel: int = 10,
                         max_rel: int = 90,
                         allowed_postags:
                         list = ['NOUN', 'ADJ', 'VERB', 'ADV'],
                         log_dir: str ='logging/'
                         ) -> list[spacyToken]:
    """Creating a list of eligible spaCy tokens (by pruning and POS-tags).

    Args:
        corpus (Corpus.corpus) : Corpus with spaCy-Doc and quantities (characters, words, and word tokens) for every text-version
        min_rel (int) : Minimum percentage of documents in which the token must exist
        max_rel (int) : Maximum percentage of documents in which the token must exist
        allowed_postags (list): https://spacy.io/api/annotation
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        filtered_tokens (list) : List of eligible spacy.tokens.token.Token
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    text_number = 0
    uniques_dict = {}
    logger.info('Starting to create a list of all tokens')
    for text in corpus:
        for version in text['versions']:
            text_number += 1
            token_list = []
            for token in version['spaCy_Doc']:
                if token.pos_ in allowed_postags:
                    token_list.append(token.lemma_)
            uniques_token_list= list(dict.fromkeys(token_list))
            logger.info('Unique tokens for text #' + str(text_number) + ': ' + str(len(uniques_token_list)))
            uniques_dict.update({str(text_number): uniques_token_list})
    logger.info('Starting to calculate minimum and maximum percantage in absolute numbers.')
    filtered_lemmas = []
    min_abs = text_number*min_rel*0.01
    max_abs = text_number*max_rel*0.01
    logger.info('Starting checking existence of unique tokens in documents')
    lemmas_dict = {}
    for lemma_list in uniques_dict.values():
        for lemma in lemma_list:
            if lemma in lemmas_dict:
                lemmas_dict.update({lemma: lemmas_dict[lemma] + 1})
            else:
                lemmas_dict.update({lemma: 1})
    for lemma in lemmas_dict:
        if min_abs < lemmas_dict[lemma] < max_abs:
            filtered_lemmas.append(lemma)
    total_number_of_lemmas = len(filtered_lemmas)
    logger.info('Total number of unique tokens: ' + str(total_number_of_lemmas))
    logger.info('function finished')
    return filtered_lemmas

"""****************************************************
    Corpus Classes
    ~~~~~~~~~~~~~~~~~~~
****************************************************"""

class Corpus:
    """Corpus Class (pre-)process processed JSON-Data for different kinds of analyses using spaCy.

    Args:
        data (dict) : Dictionary consisting of processed JSON-data (TeX-Encoding converted to plain text) and metadata about the requested data
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **pipekwargs () : This parameter allows to use optional keyword-arguments for spacyNLP.pipe (https://spacy.io/api/language#pipe)

    Properties:
        Corpus.corpus : Data updated with spaCy-Doc and quantities (characters, words, and word tokens) for every text-version

    Class-Functions:
        Corpus.filteredCorpus : Corpus updated with a filtered list of spacy.tokens.token.Token for every text-version
        Corpus.forTopicModeling : Data for topic modelling (titles and bag of words, gensim dictionary, term document frequencies)
        Corpus.specificFiltered : Returns a specific text including metadata, spaCy-Doc, and a filtered list of lemmatized words
        Corpus.specific_text : Returns a specific text including metadata and spaCy-Doc
        Corpus.statistics : Metadata, word counts, and character counts for all texts stored in a list. Output will be saved as excel.

    """
    def __init__(self,
                 data: helpers.RequestedDataDict,
                 nlp: spacyNLP,
                 log_dir: str ='logging/',
                 **pipekwargs
                 ):
        self.log_dir = log_dir
        self.corpus = copy.deepcopy(data)
        self.nlp = nlp
        self.logger = helpers.logging_init(self.log_dir)
        self.logger.info('Init corpus.Corpus')
        self._spaCyCorpus(**pipekwargs)
        self._quantities()
        self._updateMetadata()

    def _quantities(self):
        for text in self.corpus['works']:
            for version in text['versions']:
                words_number = count_words(version['data'])
                characters_number = len(version['data'])
                word_tokens_number = count_word_tokens(version['spaCy_Doc'])
                quantities_dict = {
                    'quantities' : {
                        'characters' : characters_number,
                        'words' : words_number,
                        'word_tokens' : word_tokens_number
                        }
                }
                version.update(quantities_dict)

    def _spaCyCorpus(self, **pipekwargs):
        self.logger.info('_spaCyCorpus started')
        texts_list = []
        for text in self.corpus['works']:
            for version in text['versions']:
                text_tuple = (
                    version['data'],
                    {
                        'title' : text['metadata']['title'],
                        'version' : version['metadata']['title']
                    }
                )
                texts_list.append(text_tuple)
        spaCy_Doc_list = helpers.make_spacy_docs(self.nlp, texts_list, self.log_dir, **pipekwargs)
        for spaCy_Doc in spaCy_Doc_list:
            for work in self.corpus['works']:
                if spaCy_Doc._.metadata['title'] in work['metadata']['title']:
                    for version in work['versions']:
                        if spaCy_Doc._.metadata['version'] in version['metadata']['title']:
                            version.update({'spaCy_Doc' : spaCy_Doc})

    def _updateMetadata(self):
        self.corpus['metadata'].update({'type': 'Corpus'})

    def filteredCorpus(self,
                       mode: dict | None = None,
                       allowed_postags: list | None = None
                       ) -> helpers.FilteredCorpusDict:
        """
        Args:
            mode (dict | None) : Dictionary which configures the mode with the keys 'mode' (required, value: 'pruning' or 'stopwords'), 'min_docs' (optional, value: int), 'max_docs' (optional, value: int), 'stopwords_file' (optional, value: str); or None instead of this dict will
            allowed_postags (list | None): List of https://spacy.io/api/annotation or None
        """
        self.logger.info('function started')
        filteredMetadata = copy.deepcopy(self.corpus['metadata'])
        filteredMetadata.update({'type': 'FilteredCorpus'})
        filteredWorks = filter_corpus(self.corpus['works'], mode, allowed_postags, self.log_dir)
        filteredCorpus = {
            'metadata' : filteredMetadata,
            'works' : filteredWorks
        }
        self.logger.info('function finished')
        return filteredCorpus

    def forTopicModeling(self,
                         mode: dict | None = None,
                         lowercase: bool = False,
                         allowed_postags: list = None
                         ) -> helpers.ForTopicModelingDict:
        """
        Args:
            mode (dict | None) : Dictionary which configures the mode with the keys 'mode' (required, value: 'pruning' or 'stopwords'), 'min_docs' (optional, value: int), 'max_docs' (optional, value: int), 'stopwords_file' (optional, value: str); or None instead of this dict will
            lowercase (boolean) : If true, lemmatized words will normalized
            allowed_postags (list): https://spacy.io/api/annotation
        """
        self.logger.info('function started')
        if allowed_postags == None:
            self.logger.info('default POS-tags')
            allowed_postags = ['NOUN', 'ADJ', 'VERB', 'ADV']
        tmCorpus = self.filteredCorpus(mode, allowed_postags)
        titles_lemmatized = []
        for text in tmCorpus['works']:
            for version in text['versions']:
                title = text['metadata']['title'] + '_' + version['metadata']['title']
                lemmatized = helpers.lemmas_from_tokens(version['filtered'], lowercase)
                titles_lemmatized.append({
                    'BagOfWords' : lemmatized,
                    'title' : title
                    })
        bag_of_words = helpers.get_all_bag_of_words(titles_lemmatized)
        gensim_dictionary = gensim.corpora.Dictionary(bag_of_words)
        term_document_frequency = [gensim_dictionary.doc2bow(text) for text in bag_of_words]
        DataforTopicModeling = {
            'GensimDictionary' : gensim_dictionary,
            'TermDocumentFrequency' : term_document_frequency,
            'TitlesBagOfWords' : titles_lemmatized

        }
        self.logger.info('function finished')
        return DataforTopicModeling

    def specificFiltered(self,
                         title_list: list[helpers.TitleDict],
                         mode: dict | None = None,
                         allowed_postags=None
                         ) -> list[helpers.FilteredCorpusWorksDict]:
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and ('VersionTitle')
            mode (dict | None) : Dictionary which configures the mode with the keys 'mode' (required, value: 'pruning' or 'stopwords'), 'min_docs' (optional, value: int), 'max_docs' (optional, value: int), 'stopwords_file' (optional, value: str); or None instead of this dict will
            allowed_postags (list): https://spacy.io/api/annotation
        """
        self.logger.info('function started')
        if allowed_postags == None:
            self.logger.info('default POS-tags')
            allowed_postags = ['NOUN', 'ADJ', 'VERB', 'ADV']
        specificFilteredCorpus = []
        for title in title_list:
            title_dict = {}
            for text in self.corpus['works']:
                if text['metadata']['title'] == title['TextTitle']:
                    for version in text['versions']:
                        if version['metadata']['title'] == title['VersionTitle']:
                            title_dict.update({'metadata' : text['metadata']})
                            versions = [version]
                            title_dict.update({'versions' : versions})
            specificFilteredCorpus.append(title_dict)
        self.logger.info('specific corpus created')
        specificFilteredCorpus = filter_corpus(specificFilteredCorpus, mode, allowed_postags, self.log_dir)
        self.logger.info('function finished')
        return specificFilteredCorpus

    def specificText(self, title_list: list[helpers.TitleDict]) -> list[helpers.CorpusWorksDict]:
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and ('VersionTitle')
        """
        self.logger.info('function started')
        specificCorpus = []
        for title in title_list:
            title_dict = {}
            for text in self.corpus['works']:
                if text['metadata']['title'] == title['TextTitle']:
                    for version in text['versions']:
                        if version['metadata']['title'] == title['VersionTitle']:
                            title_dict.update({'metadata' : text['metadata']})
                            versions = [version]
                            title_dict.update({'versions' : versions})
            specificCorpus.append(title_dict)
        self.logger.info('function finished')
        return specificCorpus

    def statistics(self) -> helpers.DataFramesListDict:
        self.logger.info('function started')
        quantities = {}
        i = 1
        for text in self.corpus['works']:
            for version in text['versions']:
                data = {}
                data.update({'title' : text['metadata']['title'] + ' ' + version['metadata']['title']})
                data.update({'year' : text['metadata']['year']})
                data.update(version['quantities'])
                quantities.update({i : data})
                i += 1
        df = pd.DataFrame(quantities).T.reindex(columns = ['year', 'title', 'characters', 'words', 'word_tokens'])
        statistics = {
            'title' : 'Statistics Corpus' + self.corpus['metadata']['request_date'],
            'dataframes' : [
                {
                'df' : df,
                'df_title' : 'Quantities',
                'index' : True
                }
            ]
        }
        self.logger.info('function finished')
        return statistics