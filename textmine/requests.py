# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de> and Uwe Sikora <sikora@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Corpus Module to provide Functions and Classes to request data for analyses from REST-APIs.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-01-21
Version : 1.0: 2022-10-17

Notes   :
    - Ready for review by Uwe Sikora
"""

from colorama import *
from datetime import datetime
from pylatexenc.latex2text import LatexNodes2Text
import requests

from . import helpers

"""****************************************************
    Requests Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def ask4parameter(inputtext: str,
                  delimiter: str,
                  parameter: str,
                  successmessage:str,
                  errormessage: str,
                  log_dir: str ='logging/'
                  ) -> (dict[str, list[str]] | None):
    """Asking the user for zero, one, more values for one parameter. Returns the user input as a dictionary or as None.

    Args:
        inputtext (string) : Text that tells the user the kind of expected input
        delimiter (string) : Character which separates the user input
        parameter (string) : Name of the parameter
        successmessage (string) : Text when the paramter-input is valid
        errormessage (string)   : Text when there is no valid parameter-input
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        parameters_dict (dict) : parameter and values from user input as a dictionary
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    parameters = input(inputtext)
    parameters_list = parameters.split(delimiter)
    try:
        parameters[0]
        logger.info('Parameter-Input is valid: Starting to create a dictionary.')
        parameters_dict = {parameter: parameters_list}
        print(Fore.BLUE + successmessage + '"' + '", "'.join(parameters_dict[parameter]) + '"' + Style.RESET_ALL)
        logger.info('function finished with the following parameters as result: ' + str(parameters_dict))
        return parameters_dict
    except:
        logger.info('No valid parameter-input')
        print(Fore.RED + errormessage + Style.RESET_ALL)
        logger.info('function finished without result')

def ask4parameterKMGtitle(log_dir: str ='logging/') -> (dict[str, list[str]] | None):
    """Wrapper function for ask4parameter which is specified for the title-parameter of the KMG-API.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        dict : title-parameter and title-values from user input as a dictionary
    """
    inputtext = 'Bitte geben Sie die Titel der gewünschten Texte ein (so wie im tei:titleStmt, aber ohne Elemente; mehrere Titel mit # voneinander trennen, Beispiel: "Soziale Arbeit heute#Vergessene Zusammenhänge"; wenn der Titel egal ist, lassen Sie dieses Feld leer): '
    delimiter = '#'
    parameter = 'title'
    successmessage = 'Eingegebene Titel: '
    errormessage = 'Keine Eingabe eines Titels'
    return ask4parameter(inputtext, delimiter, parameter, successmessage, errormessage, log_dir=log_dir)

def ask4parameterKMGversions(log_dir: str ='logging/') -> (dict[str, list[str]] | None):
    """Wrapper function for ask4parameter which is specified for the fassungen-parameter of the KMG-API.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        dict : fassungen-parameter and fassungen-values from user input as a dictionary
    """
    inputtext = 'Bitte geben Sie die IDs der erlaubten Fassungen ein (getrennt durch Leerzeichen, Beispiel: "a b d"; wenn Sie alle Fassungen analysieren wollen, lassen Sie dieses Feld leer): '
    delimiter = ' '
    parameter = 'fassungen'
    successmessage = 'Erlaubte Fassungen: '
    errormessage = 'Keine Eingabe der erlaubte Fassungen'
    return ask4parameter(inputtext, delimiter, parameter, successmessage, errormessage, log_dir=log_dir)

def ask4parameterKMGyears(log_dir: str ='logging/') -> (dict[str, list[str]] | None):
    """Asking the user for zero, one, more years, or a span of years. Returns the user input as a list.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        dict : year-parameter and year-values from user input as a dictionary
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    years = input('Bitte geben Sie die Erstveröffentlichungsjahre der gewünschten Texte ein (getrennt durch Leerzeichen, Beispiel: "1959 1973 1988"; auch Zeispannen sind möglich, Beispiel: "1959-1966"; wenn Sie Texte aus allen Jahren analysieren wollen, lassen Sie dieses Feld leer): ')
    years_list = years.split()
    try:
        years_list[0]
        logger.info('Parameter-Input is valid: Starting to create a dictionary.')
        years_dict = dict.fromkeys(years_list)
        for year in years_list:
            if '-' in year:
                yearsspan_list = year.split('-')
                beginning = int(yearsspan_list[0])
                end = int(yearsspan_list[1])
                timespan =  {}
                y = beginning
                while y <= end:
                    timespan.update({str(y): None})
                    y += 1
                years_dict.pop(year)
                years_dict.update(timespan)
            else:
                ()
        years_list = list(years_dict)
        years_list.sort()
        years_dict = {'year' : years_list}
        print(Fore.BLUE + 'Erlaubte Veröffentlichungsjahre: "' + '", "'.join(years_dict['year']) + '"' + Style.RESET_ALL)
        logger.info('requests.ask4parameterKMGyears finished with the following parameters as result: ' + str(years_dict))
        return years_dict
    except:
        logger.info('No valid parameter-input')
        print(Fore.RED + 'Keine Eingabe der gewünschten Jahre' + Style.RESET_ALL)
        logger.info('function finished without result')

def params_dict(params_tuple: tuple[dict, ...], log_dir: str ='logging/') -> dict:
    """Converting Tuple of parameter-dictionaries into one dictionary.

    Args:
        params_tuple (tuple) : Tuple of parameter-dictionaries
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        params_dict (dict) : Query parameters for the REST-API as dictionary {"parameter1" : "value1", "parameter2" : "value2"}
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    params_dict = {}
    for param in params_tuple:
        if type(param) is dict: params_dict.update(param)
    logger.info('function finished')
    return params_dict

def requestJSON(api_url: str,
                username: str | None,
                password: str | None,
                params: dict | None = None,
                log_dir: str ='logging/'
                ):
    """Requesting JSON-Data from a REST-API.

    Args:
        api_url (str)           : URL of the API from where the data shall be requested
        username (str | None)   : Username for the API-authentification
        password (str | None)   : Password for the API-authentification
        params (dict | None)    : Query parameters for the REST-API as dictionary ("parameter" : "value"); default value is None
        log_dir (str)           : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        json (JSON) : JSON-Data retrieved from REST-API
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if username == None and password == None:
        logger.info('Requesting data from database without username and password')
        json = requests.get(api_url, params=params)
        logger.info('Requested data from ' + json.url)
        if not json.ok:
            logger.warning('Requesting data from database failed with status code ' + str(json.status_code))
        logger.info('function finished')
        return json
    else:
        logger.info('Requesting data from database with username and password')
        json = requests.get(api_url, auth=(username, password), params=params)
        logger.info('Requested data from ' + json.url)
        if not json.ok:
            logger.warning('Requesting data from database failed with status code ' + str(json.status_code))
        logger.info('function finished')
        return json

def tex2text(txt: str, log_dir: str ='logging/') -> str:
    """Replacing the first TeX occurence in a string (marked by '//TEX/' and '/TEX//') with plain text.

    Args:
        txt (string) : Plain text that may contain TeX-Encoding (marked by '//TEX/' and '/TEX//')
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        string : String in which TeX-Encoding has been converted to plain text
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    start_keyword = '//TEX/'
    end_keyword = '/TEX//'
    start_keyword_index = txt.find(start_keyword)
    end_keyword_index = txt.find(end_keyword)
    latex = txt[(start_keyword_index + 6):(end_keyword_index)]
    plain_text = LatexNodes2Text().latex_to_text(latex)
    new_txt = txt.replace(start_keyword + latex + end_keyword, plain_text + ' ')
    logger.info('function finished')
    return new_txt

def tex_processing(txt: str, log_dir: str ='logging/') -> str:
    """Iterating throug a string and calling tex2text for every instance of TeX (marked by '//TEX/' and '/TEX//').

    Args:
        txt (string) : Plain text that may contain TeX-Encoding (marked by '//TEX/' and '/TEX//')
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        string : String in which TeX-Encoding has been converted to plain text
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if '//TEX/' in txt:
        occurences = txt.count('//TEX/')
        for number in range(occurences):
            txt = tex2text(txt)
        logger.info('function finished')
        return txt
    else:
        logger.info('function finished')
        return txt

def convertTEX(corpus: list[helpers.WorksDict], log_dir: str ='logging/') -> list[helpers.WorksDict]:
    """Converting TeX-instances in a corpus of texts (based on retrieval from an API provided by SUB's digital editions) into text.

    Args:
        list : Corpus (including texts and metadata) as a list of dictionaries
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        list : Corpus (including texts and metadata) as a list of dictionaries, any instances of TeX being converted to text
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    for text in corpus:
        for version in text['versions']:
                processed = tex_processing(version['data'])
                version.update({'data': processed})
    logger.info('function finished')
    return corpus

"""****************************************************
    Requests Classes
    ~~~~~~~~~~~~~~~~~~~
****************************************************"""
class SUBEditions:
    """SUBEditions Class to request JSON data from an API provided by SUB's digital editions.

    Args:
        api_url (str)           : URL of the API from where the data shall be requested
        param (dict)            : Query parameter for the REST-API as dictionary {"parameter" : "value"}; it is possible to insert zero, one, or more parameters
        username (str | None)   : Username for the API-authentification; Default is None
        password (str | None)   : Password for the API-authentification; Default is None
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Properties:
        SUBEditions.data : Dictionary consisting of processed JSON-data and metadata about the requested data

    """
    def __init__(self,
                 api_url: str,
                 *param: dict,
                 username: str | None = None,
                 password: str | None = None,
                 log_dir: str ='logging/'
                 ):
        self.log_dir = log_dir
        self.logger = helpers.logging_init(self.log_dir)
        self.logger.info('Init requests.SUBEditions')
        self.api_url = api_url
        self.param = param
        self.username = username
        self.password = password

        self.params = params_dict(self.param)
        self.date = datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        self.json = requestJSON(self.api_url, self.username, self.password, self.params, self.log_dir)

    @property
    def data(self) -> helpers.RequestedDataDict:
        self.logger.info('Calling requests.KMGAPI.data')
        works = self.json.json()
        data = {
            'metadata' : {
                'request_date' : self.date,
                'request_url' : self.json.url,
                'type' : 'SUBEditions data'
            },
            'works' : works
        }
        return data

class KMGAPI(SUBEditions):
    """KMGAPI Class to request JSON data from an API provided by the Klaus-Mollenhauer-Gesamtausgabe (KMG). This is an subclass of SUBEditions and provides conversion of LaTeX-Encodeing to plain text.

    Args:
        api_url (str) : URL of the API from where the data shall be requested
        param (dict) : Query parameter for the REST-API as dictionary {"parameter" : "value"}; it is possible to insert zero, one, or more parameters
        username (str | None)   : Username for the API-authentification; Default is None
        password (str | None)   : Password for the API-authentification; Default is None
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Properties:
        KMGAPI.data : Dictionary consisting of processed JSON-data (TeX-Encoding converted to plain text) and metadata about the requested data

    """

    @property
    def data(self) -> helpers.RequestedDataDict:
        self.logger.info('Calling requests.KMGAPI.data')
        works = convertTEX(self.json.json())
        data = {
            'metadata' : {
                'request_date' : self.date,
                'request_url' : self.json.url,
                'type' : 'SUBEditions data'
            },
            'works' : works
        }
        return data
