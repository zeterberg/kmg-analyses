# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Module to provide user-inputs for modes for textsPY.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-11-23
Version : 1.0: 2022-11-23

"""

from colorama import *
from typing import Literal

from textmine import helpers

"""****************************************************
    Modes Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def mode_cooccurrences(log_dir: str ='logging/') -> tuple[Literal['document', 'paragraph', 'sentence', 'span'] | None, int | None]:
    """Creates mode-arguments for word_analyses.Words.corpus_cooccurrences from user-input.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        mode (str | None) : Mode as string or None
        context_range (int | None) : Context_range as int or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    mode = input('Bitte geben Sie den "Modus" ein. Mögliche Werte sind: "document" (Dokument), "paragraph" (Absatz), "sentence" (Satz) oder "span" (Textspanne): ')
    context_range = None
    if mode == 'document':
        print(Fore.GREEN + 'Gewählter Modus: "Dokument"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    elif mode == 'paragraph':
        print(Fore.GREEN + 'Gewählter Modus: "Absatz"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    elif mode == 'sentence':
        print(Fore.GREEN + 'Gewählter Modus: "Satz"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    elif mode == 'span':
        try:
            context_range = int(input('Bitte geben Sie die Größe der Textspanne (Anzahl an Wörtern als natürliche Zahl) ein: '))
            print(Fore.GREEN + 'Gewählter Modus: "Textspanne" (' + str(context_range) + ' Wörter)' + Style.RESET_ALL)
        except:
            print(Fore.RED + 'Ungültige Eingabe. Größe der Textspanne ist nicht definiert. Wenn Sie die Eingabe nicht wiederholen, wird der Default-Wert (5 Wörter) verwendet' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    else:
        print(Fore.RED + 'Ungültige Eingabe. Kein gültiger Modus ausgewählt. Bitte Eingabe erneut starten.' + Style.RESET_ALL)
        mode = None
        logger.info('function finished')
        return mode, context_range

def mode_dependencies(log_dir: str ='logging/') -> Literal['subordinate', 'superior'] | None:
    """Creates mode-arguments for word_analyses.Words.corpus_dependencies from user-input.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        mode (str) : Mode as string
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    mode = input('Bitte geben Sie den "Modus" ein. Mögliche Werte sind: "subordinate" (untergeordnete Wörter) oder "superior" (übergeordnete Wörter): ')
    if mode == 'subordinate':
        print(Fore.GREEN + 'Gewählter Modus: "untergeordnete Wörter"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode
    elif mode == 'superior':
        print(Fore.GREEN + 'Gewählter Modus: "übergeordnete Wörter"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode
    else:
        print(Fore.RED + 'Ungültige Eingabe. Kein gültiger Modus ausgewählt. Bitte Eingabe erneut starten.' + Style.RESET_ALL)
        mode = None
        logger.info('function finished')
        return mode

def mode_word_in_context(log_dir: str ='logging/') -> tuple[Literal['paragraph', 'sentence', 'span'] | None, int | None]:
    """Creates mode-arguments for word_analyses.Words.corpus_context from user-input.

    Args:
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        mode (str | None) : Mode as string or None
        context_range (int | None) : Context_range as int or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    mode = input('Bitte geben Sie den "Modus" ein. Mögliche Werte sind: "paragraph" (Absatz), "sentence" (Satz) oder "span" (Textspanne): ')
    context_range = None
    if mode == 'paragraph':
        print(Fore.GREEN + 'Gewählter Modus: "Absatz"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    elif mode == 'sentence':
        print(Fore.GREEN + 'Gewählter Modus: "Satz"' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    elif mode == 'span':
        try:
            context_range = int(input('Bitte geben Sie die Größe der Textspanne (Anzahl an Wörtern als natürliche Zahl) ein: '))
            print(Fore.GREEN + 'Gewählter Modus: "Textspanne" (' + str(context_range) + ' Wörter)' + Style.RESET_ALL)
        except:
            print(Fore.RED + 'Ungültige Eingabe. Größe der Textspanne ist nicht definiert. Wenn Sie die Eingabe nicht wiederholen, wird der Default-Wert (5 Wörter) verwendet' + Style.RESET_ALL)
        logger.info('function finished')
        return mode, context_range
    else:
        print(Fore.RED + 'Ungültige Eingabe. Kein gültiger Modus ausgewählt. Bitte Eingabe erneut starten.' + Style.RESET_ALL)
        mode = None
        logger.info('function finished')
        return mode, context_range