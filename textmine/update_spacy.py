# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Module to update spaCy-Docs.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-09-27
Version : 1.0: 2022-10-17

"""

import copy
import spacy
from spacy.tokens.doc import Doc as spacyDoc
from spacy.language import Language as spacyNLP
from typing import TypedDict

from . import helpers

"""****************************************************
    Update spaCy TypedDict Classes
    ~~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

class RetokenizerTermsDict(TypedDict):
    terms: tuple[str, ...]
    lemma: str

"""****************************************************
    Update spaCy Module Variables
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

factories = (
    {
        'factory_name' : 'lemma_fixer',
        'name' : 'LemmaFixer',
        'after' : 'lemmatizer'
    },
    {
        'factory_name' : 'retokenizer_ngrams',
        'name' : 'RetokenizerNGrams'
    },
    {
        'factory_name' : 'paragraph_endings',
        'name' : 'ParagraphEndings'
    }
)

lemma_terms = {
        # List of words to be re-lemmatised
        # (the idea is to add frequent words here to keep the list short;
        # not every uncommon word spacy has trouble with):
        # word : { POS tag : new lemma }
        # dies
        "dies" : dict.fromkeys(["DET"], "dies"),
        "diese" : dict.fromkeys(["DET"], "dies"),
        "diesem" : dict.fromkeys(["DET"], "dies"),
        "diesen" : dict.fromkeys(["DET"], "dies"),
        "dieser" : dict.fromkeys(["DET"], "dies"),
        "dieses" : dict.fromkeys(["DET"], "dies"),
        # ein
        "ein" : dict.fromkeys(["DET"], "ein"),
        "eine" : dict.fromkeys(["DET"], "ein"),
        "einem" : dict.fromkeys(["DET"], "ein"),
        "einen" : dict.fromkeys(["DET"], "ein"),
        "einer" : dict.fromkeys(["DET"], "ein"),
        "eines" : dict.fromkeys(["DET"], "ein"),
        # erzieherisch
        "erzieherisch" : dict.fromkeys(["ADJ", "ADV"], "erzieherisch"),
        "erzieherische" : dict.fromkeys(["ADJ", "ADV"], "erzieherisch"),
        "erzieherischen" : dict.fromkeys(["ADJ", "ADV"], "erzieherisch"),
        # Erziehungsaufgabe
        "erziehungsaufgabe" : dict.fromkeys(["NOUN"], "Erziehungsaufgabe"),
        "erziehungsaufgaben" : dict.fromkeys(["NOUN"], "Erziehungsaufgabe"),
        # Erziehungsbegriff
        "erziehungsbegriff" : dict.fromkeys(["NOUN"], "Erziehungsbegriff"),
        "erziehungsbegriffs" : dict.fromkeys(["NOUN"], "Erziehungsbegriff"),
        # Erziehungsbereich
        "erziehungsbereich" : dict.fromkeys(["NOUN"], "Erziehungsbereich"),
        "erziehungsbereiche" : dict.fromkeys(["NOUN"], "Erziehungsbereich"),
        # Erziehungsfeld
        "erziehungsfeld" : dict.fromkeys(["NOUN"], "Erziehungsfeld"),
        "erziehungsfelder" : dict.fromkeys(["NOUN"], "Erziehungsfeld"),
        "erziehungsfeldes" : dict.fromkeys(["NOUN"], "Erziehungsfeld"),
        "erziehungsfelds" : dict.fromkeys(["NOUN"], "Erziehungsfeld"),
        # Erziehungsphänomen
        "erziehungsphänomen" : dict.fromkeys(["NOUN"], "Erziehungsphänomen"),
        "erziehungsphänomene" : dict.fromkeys(["NOUN"], "Erziehungsphänomen"),
        "erziehungsphänomenen" : dict.fromkeys(["NOUN"], "Erziehungsphänomen"),
        # Erziehungsplan
        "erziehungsplan" : dict.fromkeys(["NOUN"], "Erziehungsplan"),
        "erziehungsplans" : dict.fromkeys(["NOUN"], "Erziehungsplan"),
        # Erziehungsprozess
        "erziehungsprozess" : dict.fromkeys(["NOUN"], "Erziehungsprozess"),
        "erziehungsprozesse" : dict.fromkeys(["NOUN"], "Erziehungsprozess"),
        "erziehungsprozeß" : dict.fromkeys(["NOUN"], "Erziehungsprozess"),
        "erziehungsprozeße" : dict.fromkeys(["NOUN"], "Erziehungsprozess"),
        # Erziehungswissenschaftler
        "erziehungswissenschaftler" : dict.fromkeys(["NOUN"], "Erziehungswissenschaftler"),
        "erziehungswissenschaftlern" : dict.fromkeys(["NOUN"], "Erziehungswissenschaftler"),
        "erziehungswissenschaftlers" : dict.fromkeys(["NOUN"], "Erziehungswissenschaftler"),
        # erziehungswissenschaftlich
        "erziehungswissenschaftlich" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftliche" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlichem" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlichen" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlicher" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlichere" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlicherem" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftlicheren" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        "erziehungswissenschaftliches" : dict.fromkeys(["ADJ", "ADV"], "erziehungswissenschaftlich"),
        # Erziehungsvorgang
        "erziehungsvorgang" : dict.fromkeys(["NOUN"], "Erziehungsvorgang"),
        "erziehungsvorgänge" : dict.fromkeys(["NOUN"], "Erziehungsvorgang"),
        # Erziehungssystem
        "erziehungssystem" : dict.fromkeys(["NOUN"], "Erziehungssystem"),
        "erziehungssysteme" : dict.fromkeys(["NOUN"], "Erziehungssystem"),
        "erziehungssystems" : dict.fromkeys(["NOUN"], "Erziehungssystem"),
        # Freizeiten
        "freizeiten" : dict.fromkeys(["NOUN"], "Freizeit"),
        # müssen
        "muss" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musst" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musste" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mussten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musstest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musstet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsse" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssen" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsset" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsst" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsste" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsstest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsstet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "muß" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußt" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußte" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müße" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßen" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßt" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßte" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßtet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        # pädagogisch
        "pädagogische" : dict.fromkeys(["ADJ"], "pädagogisch"),
        # sollen
        "soll" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solle" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollen" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollst" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollt" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollte" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollten" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solltest" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solltet" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        # Verhalten
        "verhalten" : dict.fromkeys(["NOUN"], "Verhalten"),
        "verhaltens" : dict.fromkeys(["NOUN"], "Verhalten"),
        # wollen
        "will" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "willst" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolle" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollen" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollt" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollte" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollten" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolltest" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolltet" : dict.fromkeys(["AUX", "VERB"], "wollen")
    }

retokenizer_terms = [
    {
        'terms': ('18.', 'Jahrhundert'),
        'lemma': '18. Jahrhundert',
    },
    {
        'terms': ('19.', 'Jahrhundert'),
        'lemma': '19. Jahrhundert',
    },
    {
        'terms': ('20.', 'Jahrhundert'),
        'lemma': '20. Jahrhundert',
    }
]

"""****************************************************
    Update spaCy Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def update_spacyNLP(nlp: spacyNLP,
                    factories_tuple: tuple[dict, ...] = factories,
                    log_dir: str ='logging/'
                    ) -> spacyNLP:
    """Updates spaCy Processing Pipeline.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        factories_tuple (tuple) : Tuple consisting of dictionaries with keyword-arguments for nlp.add_pipe(). Each dictionary must at least contain the key 'factory_name'. For other possible keyword-arguments see https://spacy.io/api/language#add_pipe. This parameter allows to add custom components to the nlp-pipeline. Default: LemmaFixer, RetokenizerNGrams, and ParagraphEndings (all defined in this module).
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    for factory_dict in factories_tuple:
            nlp.add_pipe(**factory_dict)
    logger.info('function finished')
    return nlp

"""****************************************************
    Update spaCy Module Classes
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

try:
    spacy.registry.get('factories', 'lemma_fixer')
except:
    @spacyNLP.factory('lemma_fixer', default_config={'terms': lemma_terms, 'log_dir': None})
    def create_lemmaFixer_component(nlp: spacyNLP,
                                    name: str,
                                    terms: dict[str, dict],
                                    log_dir: str | None):
        return LemmaFixer(nlp, terms, log_dir)

class LemmaFixer:
    """Fixes wrong lemmas from the built-in spacy lemmatizer.
        Spacy basically lemmatises using this list:
            https://raw.githubusercontent.com/explosion/spacy-lookups-data/master/spacy_lookups_data/data/de_lemma_lookup.json

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        terms (dict) : List of words to be re-lemmatised
        log_dir (str or None) : Directory to save log-file, default is the new folder 'logging' in the current working directory; if None: no Logger will be applied

    Returns:
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc

    """
    def __init__(self,
                 nlp: spacyNLP,
                 terms: dict[str, dict]=lemma_terms,
                 log_dir: str | None = 'logging/'):
        self.terms = terms
        if not log_dir is None:
            self.logger = helpers.logging_init(log_dir)

    def __call__(self, spaCy_Doc: spacyDoc) -> spacyDoc:
        try:
            self.logger.info('Lemma fixing started')
        except:
            pass
        for token in spaCy_Doc:
            if token.lower_ in self.terms and token.pos_ in self.terms[token.lower_]:
                token.lemma_ = self.terms[token.lower_][token.pos_]
        try:
            self.logger.info('Lemma fixing finished')
        except:
            pass
        return spaCy_Doc

try:
    spacy.registry.get('factories', 'retokenizer_ngrams')
except:
    @spacyNLP.factory('retokenizer_ngrams', default_config={'terms_dict_list': retokenizer_terms, 'log_dir': None}, retokenizes=True)
    def create_retokenizer_component(nlp: spacyNLP,
                                    name: str,
                                    terms_dict_list: list[RetokenizerTermsDict],
                                    log_dir: str | None):
        return RetokenizerNGrams(nlp, terms_dict_list, log_dir)

class RetokenizerNGrams:
    """Retokenizes ngrams in a spaCy Doc.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        spaCy_Doc (spacy.tokens.doc.Doc) : spaCy document object
        terms_dict_list (dict) : List consisting of dictionaries with keys 'terms' (value: tuple with components of ngram in right order) and 'lemma' (value: lemma of ngram as string)
        log_dir (str or None) : Directory to save log-file, default is the new folder 'logging' in the current working directory; if None: no Logger will be applied

    Returns:
        spaCy_Doc (spacy.tokens.doc.Doc) : spaCy document object
    """
    def __init__(self,
                 nlp: spacyNLP,
                 terms_dict_list: list[RetokenizerTermsDict] = retokenizer_terms,
                 log_dir: str | None = 'logging/'):
        if not log_dir is None:
            self.logger = helpers.logging_init(log_dir)
            self.logger.info('Initialize Retokenizer')
        self.terms_dict_list = copy.deepcopy(terms_dict_list)
        for terms_dict in self.terms_dict_list:
            term_token_list = []
            for term in terms_dict['terms']:
                nlp_term = nlp(term)[0]
                term_token_list.append(nlp_term)
            terms_dict.update({'term token': term_token_list})

    def __call__(self, spaCy_Doc: spacyDoc) -> spacyDoc:
        try:
            self.logger.info('Retokenizing started')
        except:
            pass
        for token in reversed(spaCy_Doc):
            for terms_dict in self.terms_dict_list:
                term_count = len(terms_dict['term token'])
                try:
                    spaCy_Doc[term_count-1]
                except:
                    pass
                else:
                    if token.lemma_ == terms_dict['term token'][0].lemma_:
                        i = token.i
                        checksum = 0
                        for n in range(1, term_count):
                            if spaCy_Doc[i+n].lemma_ == terms_dict['term token'][n].lemma_:
                                checksum += 1
                        if term_count == checksum + 1:
                            start_i = i
                            end_i = i+term_count
                            with spaCy_Doc.retokenize() as retokenizer:
                                attrs = {'LEMMA': terms_dict['lemma']}
                                retokenizer.merge(spaCy_Doc[start_i:end_i], attrs=attrs)
        try:
            self.logger.info('Retokenizing finished')
        except:
            pass
        return spaCy_Doc

try:
    spacy.registry.get('factories', 'paragraph_endings')
except:
    @spacyNLP.factory('paragraph_endings', default_config={'log_dir': None})
    def create_paragraphEndings_component(nlp: spacyNLP,
                                          name: str,
                                          log_dir: str | None):
        return ParagraphEndings(nlp, log_dir)

class ParagraphEndings:
    """Inserts markers for paragraph endings in a spaCy Doc.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        spaCy_Doc (spacy.tokens.doc.Doc) : spaCy document object
        log_dir (str or None) : Directory to save log-file, default is the new folder 'logging' in the current working directory; if None: no Logger will be applied

    Returns:
        spaCy_Doc (spacy.tokens.doc.Doc) : spaCy document object
    """
    def __init__(self,
                 nlp: spacyNLP,
                 log_dir: str | None = 'logging/'):
        if not log_dir is None:
            self.logger = helpers.logging_init(log_dir)
        self.is_paragraph_ending_getter = lambda token: '\n' in token.text

    def __call__(self, spaCy_Doc: spacyDoc) -> spacyDoc:
        try:
            self.logger.info('Paragraph endings insertion started')
        except:
            pass
        for token in spaCy_Doc:
            token.set_extension('is_paragraph_ending', getter=self.is_paragraph_ending_getter, force=True)
        try:
            self.logger.info('Paragraph endings insertion finished')
        except:
            pass
        return spaCy_Doc