# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Module for analyses of specific words.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-05-04
Version : 1.0: 2022-11-11

"""

from collections import Counter
from colorama import *
from functools import partial
import math
from p_tqdm import p_imap
import pandas as pd
from spacy.matcher import Matcher
from spacy.matcher import DependencyMatcher
from spacy.language import Language as spacyNLP
from spacy.tokens.doc import Doc as spacyDoc
from spacy.tokens.span import Span as spacySpan
from spacy.tokens.token import Token as spacyToken
from typing import Literal

from . import helpers

"""****************************************************
    WORD ANALYSES Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def all_token_instances(spaCy_Doc_dict: helpers.SpacyDocDict,
                        token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                        log_dir: str ='logging/'
                        ) -> helpers.TokenInstancesDict:
    """Returns all instances of one lemma (optionally specified by POS-tags).

    Args:
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        all_tokens (list) : List of spacy.tokens.token.Token
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data = {}
    for token_dict in token_dict_list:
        all_tokens = []
        for token in spaCy_Doc_dict['spaCy_Doc']:
            if 'POS_tags' in token_dict:
                if token.lemma == token_dict['spaCy_Token'].lemma and token.pos_ in token_dict['POS_tags']:
                    all_tokens.append(token)
            else:
                if token.lemma == token_dict['spaCy_Token'].lemma:
                    all_tokens.append(token)
        data.update({token_dict['spaCy_Token'].lemma_ : all_tokens})
    all_token_instances = {
        'Title' : spaCy_Doc_dict['title'],
        'Data' : data
    }
    logger.info('function finished')
    return all_token_instances

def check_token(spacy: spacyDoc | spacySpan,
                token_dict: helpers.TokenDict | helpers.TokenPosDict,
                log_dir: str ='logging/'
                ) -> bool:
    """Verifies if the lemma of a token exists in a spaCy Doc or spaCy Span.

    Args:
        spacy (spacy.tokens.doc.Doc | spacy.tokens.span.Span) : https://spacy.io/api/doc or https://spacy.io/api/span
        token_dict (dict) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        (boolean) : True if the token exists in the doc, False if not
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    number = 0
    for token in spacy:
        if 'POS_tags' in token_dict:
            if token.lemma == token_dict['spaCy_Token'].lemma and token.pos_ in token_dict['POS_tags']:
                number += 1
                break
        else:
            if token.lemma == token_dict['spaCy_Token'].lemma:
                number += 1
    if number == 0:
        logger.info(f'{token_dict["spaCy_Token"]} does not exist in spacy')
        logger.info('function finished')
        return False
    else:
        logger.info(f'{token_dict["spaCy_Token"]} does exist in spacy')
        logger.info('function finished')
        return True

def collect_contexts(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                     word_context_list: list[helpers.WordContextDict | None],
                     log_dir: str ='logging/'
                     ) -> dict[str, dict[str, list[spacySpan]]]:
    """Re-arranges the data of results of word_context or words_context.

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        cooccurrences_dependencies_list (list) : List of results of word_context or words_context
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        data_dict (dict) : Re-arranged results of word_context or words_context
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data_dict = {}
    for token_dict in token_dict_list:
        data_dict.update({token_dict['spaCy_Token'].lemma_: {'Corpus': []}})
    for itm in word_context_list:
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            try:
                itm['Data'][lemma]
            except:
                pass
            else:
                itm_list = itm['Data'][lemma]
                if not itm_list == None:
                    data_dict[lemma]['Corpus'].extend(itm_list)
                    data_dict[lemma].update({itm['Title'] : itm_list})
    logger.info('function finished')
    return data_dict

def collect_cooccurrences_dependencies(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                       cooccurrences_dependencies_list: list[helpers.WordCooccurrencesDict | helpers.WordDependenciesDict | None],
                                       log_dir: str ='logging/'
                                       ) -> dict[str, dict[str, dict[str: int]]]:
    """Re-arranges the data of results of word_cooccurrences or word_dependencies.

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        cooccurrences_dependencies_list (list) : List of results of word_cooccurrences or word_dependencies
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        data_dict (dict) : Re-arranged results of word_cooccurrences or word_dependencies
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data_dict = {}
    for token_dict in token_dict_list:
        data_dict.update({token_dict['spaCy_Token'].lemma_: {'Corpus': {}}})
    for itm in cooccurrences_dependencies_list:
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            try:
                itm['Data'][lemma]
            except:
                pass
            else:
                itm_dict = itm['Data'][lemma]
                if itm_dict:
                    for itm_k in itm_dict:
                        try:
                            data_dict[lemma]['Corpus'][itm_k]
                        except:
                            data_dict[lemma]['Corpus'].update({itm_k: itm_dict[itm_k]})
                        else:
                            new_value = data_dict[lemma]['Corpus'][itm_k] + itm_dict[itm_k]
                            data_dict[lemma]['Corpus'].update({itm_k : new_value})
                    data_dict[lemma].update({itm['Title'] : itm_dict})
            sorted_corpus_dict = dict(sorted(data_dict[lemma]['Corpus'].items(), key=lambda item: item[1], reverse=True))
            data_dict[lemma].update({'Corpus' : sorted_corpus_dict})
    logger.info('function finished')
    return data_dict

def collect_frequency_weight(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                             frequency_weight_list: list[helpers.WordFrequencyWeightDict],
                             spaCy_Doc_dict_list: list[helpers.SpacyDocDict],
                             log_dir: str ='logging/'
                             ) -> dict[str, helpers.CorpusFrequencyWeightDict]:
    """Re-arranges the data of results of word_frequency_weight.

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        frequency_weight_list (list) : List of results of word_frequency_weight
        spaCy_Doc_dict_list (list) : List of dictionaries containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        data_dict (dict) : Re-arranged results of word_frequency_weight (and additionally weight for whole corpus)
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data_dict = {}
    for token_dict in token_dict_list:
        data_dict.update({token_dict['spaCy_Token'].lemma_: {'frequency': {'Corpus': 0}, 'weight': {'Corpus': 0.0}}})
    for itm in frequency_weight_list:
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            try:
                itm['Data'][lemma]
            except:
                pass
            else:
                itm_dict = itm['Data'][lemma]
                if itm_dict:
                    new_value = data_dict[lemma]['frequency']['Corpus'] + itm_dict['frequency']
                    data_dict[lemma]['frequency'].update({'Corpus' : new_value})
                    data_dict[lemma]['frequency'].update({itm['Title'] : itm_dict['frequency']})
                    data_dict[lemma]['weight'].update({itm['Title'] : itm_dict['weight']})
    sentences = []
    word_number = 0
    for spaCy_Doc_dict in spaCy_Doc_dict_list:
        sentences.extend(list(spaCy_Doc_dict['spaCy_Doc'].sents))
        word_number = word_number + spaCy_Doc_dict['word_tokens']
    for token_dict in token_dict_list:
        lemma = token_dict['spaCy_Token'].lemma_
        try:
            data_dict[lemma]['frequency']['Corpus']
        except:
            pass
        else:
            frequency = data_dict[lemma]['frequency']['Corpus']
            lemma_sentences = []
            sentence_number = 0
            for itm in sentences:
                lemma_sentences.append(itm.lemma_)
                sentence_number += 1
            tf_score = frequency/int(word_number)
            idf_score = math.log(sentence_number/sum(1 for sentence in lemma_sentences if lemma in sentence))
            tf_idf_score = tf_score * idf_score
            data_dict[lemma]['weight'].update({'Corpus' : tf_idf_score})
    logger.info('function finished')
    return data_dict

def context_paragraphs(token_dict: helpers.TokenDict | helpers.TokenPosDict | list[helpers.TokenDict | helpers.TokenPosDict],
                       spacy_Doc: spacyDoc,
                       log_dir: str ='logging/'
                       ) -> list[spacySpan] | None:
    """Returns all paragraphs which contain the given word(s).

    Args:
        token_dict (dict | list) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags or list of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spacy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        context_sentences (list | None) : List of https://spacy.io/api/span or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if type(token_dict) == list:
        logger.info('Sentences have to contain several specific words')
        max_checks = len(token_dict)
        t = 0
        for t_d in token_dict:
            if check_token(spacy_Doc, t_d):
                t += 1
        if t == max_checks:
            context_paragraphs = []
            paragraphs = helpers.get_paragraphs(spacy_Doc)
            for paragraph in paragraphs:
                n = 0
                checks = 0
                while checks < max_checks:
                    spaCy_Token = token_dict[checks]['spaCy_Token']
                    if 'POS_tags' in token_dict[checks]:
                        for token in paragraph:
                            if spaCy_Token.lemma == token.lemma and token.pos_ in token_dict[checks]['POS_tags']:
                                n += 1
                                break
                    else:
                        for token in paragraph:
                            if spaCy_Token.lemma == token.lemma:
                                n += 1
                                break
                    checks +=1
                if n == max_checks:
                    context_paragraphs.append(paragraph)
        else:
            logger.info('Document does not contain all given words')
            context_paragraphs = None
    else:
        logger.info('Sentences have to contain one specific word')
        if check_token(spacy_Doc, token_dict):
            context_paragraphs = []
            paragraphs = helpers.get_paragraphs(spacy_Doc)
            for paragraph in paragraphs:
                for token in paragraph:
                    if 'POS_tags' in token_dict:
                        if token.lemma == token_dict['spaCy_Token'].lemma and token.pos_ in token_dict['POS_tags']:
                            context_paragraphs.append(paragraph)
                            break
                    else:
                        if token.lemma == token_dict['spaCy_Token'].lemma:
                            context_paragraphs.append(paragraph)
                            break
            return context_paragraphs
        else:
            logger.info('Document does not contain the given word')
            context_paragraphs = None
    logger.info('function finished')
    return context_paragraphs

def context_sentences(token_dict: helpers.TokenDict | helpers.TokenPosDict | list[helpers.TokenDict | helpers.TokenPosDict],
                      spacy_Doc: spacyDoc,
                      log_dir: str ='logging/'
                      ) -> list[spacySpan] | None:
    """Returns all sentences which contain the given word(s).

    Args:
        token_dict (dict | list) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags or list of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spacy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        context_sentences (list | None) : List of https://spacy.io/api/span or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if type(token_dict) == list:
        logger.info('Sentences have to contain several specific words')
        max_checks = len(token_dict)
        t = 0
        for t_d in token_dict:
            if check_token(spacy_Doc, t_d):
                t += 1
        if t == max_checks:
            context_sentences = []
            for sentence in spacy_Doc.sents:
                n = 0
                checks = 0
                while checks < max_checks:
                    spaCy_Token = token_dict[checks]['spaCy_Token']
                    if 'POS_tags' in token_dict[checks]:
                        for token in sentence:
                            if spaCy_Token.lemma == token.lemma and token.pos_ in token_dict[checks]['POS_tags']:
                                n += 1
                                break
                    else:
                        for token in sentence:
                            if spaCy_Token.lemma == token.lemma:
                                n += 1
                                break
                    checks +=1
                if n == max_checks:
                    context_sentences.append(sentence)
        else:
            logger.info('Document does not contain all given words')
            context_sentences = None
    else:
        logger.info('Sentences have to contain one specific word')
        if check_token(spacy_Doc, token_dict):
            context_sentences = []
            for sentence in spacy_Doc.sents:
                for token in sentence:
                    if 'POS_tags' in token_dict:
                        if token.lemma == token_dict['spaCy_Token'].lemma and token.pos_ in token_dict['POS_tags']:
                            context_sentences.append(sentence)
                            break
                    else:
                        if token.lemma == token_dict['spaCy_Token'].lemma:
                            context_sentences.append(sentence)
                            break
        else:
            logger.info('Document does not contain the given word')
            context_sentences = None
    logger.info('function finished')
    return context_sentences

def context_spans(token_dict: helpers.TokenDict | helpers.TokenPosDict,
                  spacy_Doc: spacyDoc,
                  nlp: spacyNLP,
                  context_range: int = 5,
                  log_dir: str ='logging/'
                  ) -> list[spacySpan] | None:
    """Return all spans which contain the given word.

    Args:
        token_dict (dict) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags
        spacy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        context_range (int) : Integer for context range
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        context_spans (list | None) : List of https://spacy.io/api/span or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if check_token(spacy_Doc, token_dict):
        context_spans = []
        if 'POS_tags' in token_dict:
            pattern = [{'LEMMA' : token_dict['spaCy_Token'].lemma_, 'POS' : {'IN': token_dict['POS_tags']}}]
        else:
            pattern = [{'LEMMA' : token_dict['spaCy_Token'].lemma_}]
        matcher = Matcher(nlp.vocab)
        matcher.add("Word in Context", [pattern])
        matches = matcher(spacy_Doc)
        for match in matches:
            match_index = match[1]
            words = 1
            start_index_range = context_range
            while words <= start_index_range:
                try:
                    spacy_Doc[match_index+words]
                except:
                    break
                else:
                    if spacy_Doc[match_index-words].pos_ in ['PUNCT', 'SPACE', 'SYM', 'X']:
                        words += 1
                        start_index_range += 1
                    else:
                        words += 1
            start_index = match_index-start_index_range
            if start_index < 0:
                start_index = 0
            end_index_range = context_range
            words = 1
            while words <= end_index_range:
                try:
                    spacy_Doc[match_index+words]
                except:
                    break
                else:
                    if spacy_Doc[match_index+words].pos_ in ['PUNCT', 'SPACE', 'SYM', 'X']:
                        words += 1
                        end_index_range += 1
                    else:
                        words += 1
            end_index_range += 1
            end_index = match_index+end_index_range
            context_match = spacy_Doc[start_index:end_index]
            context_spans.append(context_match)
    else:
        logger.info('Document does not contain the given word')
        context_spans = None
    logger.info('function finished')
    return context_spans

def dependencies(nlp: spacyNLP,
                 pattern_dict: helpers.PatternDict,
                 spacy_Doc: spacyDoc,
                 log_dir: str ='logging/'
                 ) -> list[tuple[int, list[int]]]:
    """Performs dependency analysis.

    Args:
        nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
        pattern_dict (dict) : Dictionary containing 'title' (str; title of the dependency analysis) and 'patterns' (list; patterns for dependency analysis, see https://spacy.io/api/dependencymatcher)
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        dependencies (liste) : List with result of dependency analysis
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    dep_matcher = DependencyMatcher(vocab=nlp.vocab)
    dep_matcher.add(pattern_dict['title'], patterns=pattern_dict['patterns'])
    dependencies = dep_matcher(spacy_Doc)
    logger.info('function finished')
    return dependencies

def dependency_patterns_subordinate(token_dict: helpers.TokenDict | helpers.TokenPosDict,
                                    result_POS_tags: None | list[str] = None,
                                    log_dir: str ='logging/'
                                    ) -> list[dict[str, any]]:
    """Returns patterns for spaCy dependency matcher to find all subordinate dependencies.

    Args:
        token_dict (dict) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags
        result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        patterns (list) : Patterns for spaCy dependency matcher to find all subordinate dependencies
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if 'POS_tags' in token_dict:
        right_id_pattern = {'RIGHT_ID': 'word', 'RIGHT_ATTRS': {'LEMMA': token_dict['spaCy_Token'].lemma, 'POS': {"IN": token_dict['POS_tags']}}}
    else:
        right_id_pattern = {'RIGHT_ID': 'word', 'RIGHT_ATTRS': {'LEMMA': token_dict['spaCy_Token'].lemma, 'POS': {"NOT_IN": ['PUNCT', 'SPACE', 'SYM', 'X']}}}
    if result_POS_tags == None:
        patterns = [
            right_id_pattern,
            {'LEFT_ID': 'word', 'REL_OP': '>', 'RIGHT_ID': 'dependent', 'RIGHT_ATTRS': {'POS': {"NOT_IN": ['PUNCT', 'SPACE', 'SYM', 'X']}}}
        ]
    else:
        patterns = [
            right_id_pattern,
            {'LEFT_ID': 'word', 'REL_OP': '>', 'RIGHT_ID': 'dependent', 'RIGHT_ATTRS': {'POS': {"IN": result_POS_tags}}}
        ]
    logger.info('function finished')
    return patterns

def dependency_patterns_superior(token_dict: helpers.TokenDict | helpers.TokenPosDict,
                                 result_POS_tags: None | list[str] = None,
                                 log_dir: str ='logging/'
                                 ) -> list[dict[str, any]]:
    """Returns patterns for spaCy dependency matcher to find all superior dependencies.

    Args:
        token_dict (dict) : Dictionary with spacy.tokens.token.Token and optional the related POS-tags
        result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        patterns (list) : Patterns for spaCy dependency matcher to find all superior dependencies
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if 'POS_tags' in token_dict:
        right_id_pattern = {'RIGHT_ID': 'word', 'RIGHT_ATTRS': {'LEMMA': token_dict['spaCy_Token'].lemma, 'POS': {"IN": token_dict['POS_tags']}}}
    else:
        right_id_pattern = {'RIGHT_ID': 'word', 'RIGHT_ATTRS': {'LEMMA': token_dict['spaCy_Token'].lemma, 'POS': {"NOT_IN": ['PUNCT', 'SPACE', 'SYM', 'X']}}}
    if result_POS_tags == None:
        patterns = [
            right_id_pattern,
            {'LEFT_ID': 'word', 'REL_OP': '<', 'RIGHT_ID': 'head', 'RIGHT_ATTRS': {'POS': {"NOT_IN": ['PUNCT', 'SPACE', 'SYM', 'X']}}}
        ]
    else:
        patterns = [
            right_id_pattern,
            {'LEFT_ID': 'word', 'REL_OP': '<', 'RIGHT_ID': 'head', 'RIGHT_ATTRS': {'POS': {"IN": result_POS_tags}}}
        ]
    logger.info('function finished')
    return patterns

def tokens_from_string(spaCy_Doc: spacyDoc,
                       string: str,
                       spacy_token_POS_tags: list[str] | None = None,
                       log_dir: str ='logging/'
                       ) -> list[spacyToken]:
    """Creates list with all tokens which lemma contains the given string.

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc) : https://spacy.io/api/doc
        string (str) : Any kind of string
        spacy_token_POS_tags (list, optional) : List of POS-tags to specify spaCy_Token
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        tokens (list) : List of spacy.tokens.token.Token
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    word_list = []
    for token in spaCy_Doc:
        if spacy_token_POS_tags == None:
            if string.lower() in token.lemma_.lower():
                word_list.append(token)
        else:
            if string.lower() in token.lemma_.lower() and token.pos_ in spacy_token_POS_tags:
                word_list.append(token)
    tokens_list = helpers.remove_spacy_duplicates(word_list)
    logger.info('function finished')
    return tokens_list

def word_context(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                 spaCy_Doc_dict: helpers.SpacyDocDict,
                 mode: Literal['paragraph', 'sentence', 'span'] = 'sentence',
                 log_dir: str ='logging/',
                 **spankwargs: spacyNLP | int
                 ) -> helpers.WordContextDict | None:
    """Returns word in context (either paragraphs, sentences, or spans).

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        mode (str) : Defines the mode ('paragraph', 'sentence', or 'span'); default: 'sentence'
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **spankwargs () : Keyword arguments for mode='span' (see function context_spans)

    Returns:
        word_context_dict (dict) : Dictionary containing 'Data' (dictionary with list of str per token), 'Title' (str), 'Type' (str) or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if mode == 'span':
        try:
            context_spans(token_dict_list[0], spaCy_Doc_dict['spaCy_Doc'], **spankwargs)
        except:
            logger.warning('If you pass "span" as an argument for "mode", you have to provide keyword arguments for the function context_spans (at least "nlp", optionally "context_range").')
            logger.info('function finished')
            return None
    data_dict = {}
    for token_dict in token_dict_list:
        lemma = token_dict['spaCy_Token'].lemma_
        if mode == 'paragraph':
            logger.info('Context: paragraphs')
            contexts = context_paragraphs(token_dict, spaCy_Doc_dict['spaCy_Doc'])
            kind = 'context paragraph'
        elif mode == 'sentence':
            logger.info('Context: sentences')
            contexts = context_sentences(token_dict, spaCy_Doc_dict['spaCy_Doc'])
            kind = 'context sentence'
        elif mode == 'span':
            logger.info('Context: spans')
            contexts = context_spans(token_dict, spaCy_Doc_dict['spaCy_Doc'], **spankwargs)
            kind = 'context span'
        data_dict.update({lemma : contexts})
    word_context_dict = {
        'Data' : data_dict,
        'Title' : spaCy_Doc_dict['title'],
        'Type' : kind
    }
    logger.info('function finished')
    return word_context_dict

def word_cooccurences(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                      spaCy_Doc_dict: helpers.SpacyDocDict,
                      mode: Literal['document', 'paragraph', 'sentence', 'span'] = 'sentence',
                      result_POS_tags: None | list[str] = None,
                      log_dir: str ='logging/',
                      **spankwargs) -> helpers.WordCooccurrencesDict | None:
    """Returns cooccurrences for words (either in document, paragraphs, sentences, or spans).

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
        result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **spankwargs () : Keyword arguments for mode='span' (see function context_spans)

    Returns:
        word_context_dict (dict) : Dictionary containing 'Data' (dictionary with list of str per token), 'Title' (str), 'Type' (str) or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if mode == 'span':
        try:
            context_spans(token_dict_list[0], spaCy_Doc_dict['spaCy_Doc'], **spankwargs)
        except:
            logger.warning('If you pass "span" as an argument for "mode", you have to provide keyword arguments for the function context_spans (at least "nlp", optionally "context_range").')
            logger.info('function finished')
            return None
    data_dict = {}
    for token_dict in token_dict_list:
        lemma = token_dict['spaCy_Token'].lemma_
        if mode == 'document':
            logger.info('Cooccurrences in documents')
            if check_token(spaCy_Doc_dict['spaCy_Doc'], token_dict):
                if result_POS_tags == None:
                    token_list = [token.lemma_ for token in spaCy_Doc_dict['spaCy_Doc'] if token.pos_ not in ['PUNCT', 'SPACE', 'SYM', 'X'] and token.lemma != token_dict['spaCy_Token'].lemma]
                else:
                    token_list = [token.lemma_ for token in spaCy_Doc_dict['spaCy_Doc'] if token.pos_ in result_POS_tags and token.lemma != token_dict['spaCy_Token'].lemma]
            else:
                token_list = None
            kind = 'cooccurrences document'
        elif mode == 'paragraph':
            logger.info('Cooccurrences in paragraphs')
            paragraphs = context_paragraphs(token_dict, spaCy_Doc_dict['spaCy_Doc'])
            if paragraphs == None:
                token_list = None
            else:
                if result_POS_tags == None:
                    token_list = [token.lemma_ for paragraph in paragraphs for token in paragraph if token.pos_ not in ['PUNCT', 'SPACE', 'SYM', 'X'] and token.lemma != token_dict['spaCy_Token'].lemma]
                else:
                    token_list = [token.lemma_ for paragraph in paragraphs for token in paragraph if token.pos_ in result_POS_tags and token.lemma != token_dict['spaCy_Token'].lemma]
            kind = 'cooccurrences paragraph'
        elif mode == 'sentence':
            logger.info('Cooccurrences in sentences')
            sentences = context_sentences(token_dict, spaCy_Doc_dict['spaCy_Doc'])
            if sentences == None:
                token_list = None
            else:
                if result_POS_tags == None:
                    token_list = [token.lemma_ for sentence in sentences for token in sentence if token.pos_ not in ['PUNCT', 'SPACE', 'SYM', 'X'] and token.lemma != token_dict['spaCy_Token'].lemma]
                else:
                    token_list = [token.lemma_ for sentence in sentences for token in sentence if token.pos_ in result_POS_tags and token.lemma != token_dict['spaCy_Token'].lemma]
            kind = 'cooccurrences sentence'
        elif mode == 'span':
            logger.info('Cooccurrences in spans')
            spans = context_spans(token_dict, spaCy_Doc_dict['spaCy_Doc'], **spankwargs)
            if spans == None:
                token_list = None
            else:
                if result_POS_tags == None:
                    token_list = [token.lemma_ for span in spans for token in span if token.pos_ not in ['PUNCT', 'SPACE', 'SYM', 'X'] and token.lemma != token_dict['spaCy_Token'].lemma]
                else:
                    token_list = [token.lemma_ for span in spans for token in span if token.pos_ in result_POS_tags and token.lemma != token_dict['spaCy_Token'].lemma]
            kind = 'cooccurrences span'
        token_dict = dict(Counter(token_list))
        sorted_tokens_dict = helpers.sort_frequencies(token_dict)
        data_dict.update({lemma : sorted_tokens_dict})
    word_cooccurences_dict = {
        'Data' : data_dict,
        'Title' : spaCy_Doc_dict['title'],
        'Type' : kind
    }
    logger.info('function finished')
    return word_cooccurences_dict

def word_dependencies(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                      spaCy_Doc_dict: helpers.SpacyDocDict,
                      nlp: spacyNLP,
                      mode: Literal['custom', 'subordinate', 'superior'],
                      result_POS_tags: None | list[str] = None,
                      custom_patterns: None | list[dict] = None,
                      log_dir: str ='logging/'
                      ) -> helpers.WordDependenciesDict | None:
    """Returns dependencies for words (either subordinate or superior dependencies or dependencies defined by custom patterns).

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        mode (str) : Defines the mode ('custom', 'subordinate', 'superior'); 'custom' requires an argument for 'custom_patterns'
        result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
        custom_patterns (None | list) : None or Pattern for spaCy dependency matcher (https://spacy.io/api/dependencymatcher)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        word_dependencies_dict (dict) : Dictionary containing 'Data' (dictionary with list of str per token), 'Title' (str), 'Type' (str) or None
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    if mode == 'custom' and custom_patterns == None:
        logger.warning('If you pass "custom" as an argument for "mode", you have to provide an argument for paramter "custom_patterns".')
        logger.info('function finished')
        return None
    else:
        data_dict = {}
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            if check_token(spaCy_Doc_dict['spaCy_Doc'], token_dict):
                if mode == 'custom':
                    logger.info('Using custom patterns to find dependencies')
                    pattern_title = 'dependencies custom'
                    patterns = custom_patterns
                elif mode == 'subordinate':
                    logger.info('Finding subordinate dependencies')
                    pattern_title = 'dependencies subordinate'
                    patterns = dependency_patterns_subordinate(token_dict, result_POS_tags)
                elif mode == 'superior':
                    logger.info('Finding superior dependencies')
                    pattern_title = 'dependencies superior'
                    patterns = dependency_patterns_superior(token_dict, result_POS_tags)
                pattern_dict = {
                    'title': pattern_title,
                    'patterns': [patterns]
                }
                matches_list = [spaCy_Doc_dict['spaCy_Doc'][match[1][1]].lemma_ for match in dependencies(nlp, pattern_dict, spaCy_Doc_dict['spaCy_Doc'])]
                if not matches_list:
                    dependencies_dict = None
                else:
                    dependencies_dict = helpers.sort_frequencies(dict(Counter(matches_list)))
            else:
                dependencies_dict = None
                if mode == 'custom':
                    pattern_title = 'dependencies custom'
                elif mode == 'subordinate':
                    pattern_title = 'dependencies subordinate'
                elif mode == 'superior':
                    pattern_title = 'dependencies superior'
            data_dict.update({lemma : dependencies_dict})
        word_dependencies_dict = {
            'Data' : data_dict,
            'Title' : spaCy_Doc_dict['title'],
            'Type' : pattern_title
        }
        logger.info('function finished')
        return word_dependencies_dict

def word_frequency_weight(token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                          spaCy_Doc_dict: helpers.SpacyDocDict,
                          log_dir: str ='logging/'
                          ) -> helpers.WordFrequencyWeightDict:
    """Counts frequencies and calculates weights of words in one spaCy Doc.

    Args:
        token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        word_frequency_weight_dict (dict) : Dictionary containing 'Data' (dictionary with frequency and weight per token), 'Title' (str), 'Type' (str)
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data = {}
    for token_dict in token_dict_list:
        lemma = token_dict['spaCy_Token'].lemma_
        frequency = 0
        for token in spaCy_Doc_dict['spaCy_Doc']:
            if 'POS_tags' in token_dict:
                if token.lemma_ == lemma and token.pos_ in token_dict['POS_tags']:
                    frequency += 1
            else:
                if token.lemma_ == lemma:
                    frequency += 1
        if frequency != 0:
            lemma_sentences = []
            sentence_number = 0
            for itm in spaCy_Doc_dict['spaCy_Doc'].sents:
                lemma_sentences.append(itm.lemma_)
                sentence_number += 1
            tf_score = frequency/int(spaCy_Doc_dict['word_tokens'])
            idf_score = math.log(sentence_number/sum(1 for sentence in lemma_sentences if lemma in sentence))
            tf_idf_score = tf_score * idf_score
        else:
            tf_idf_score = 0
        data_dict = {
            lemma : {
                'frequency' : frequency,
                'weight' : tf_idf_score
            }
        }
        data.update(data_dict)
    word_frequency_weight_dict = {
        'Data' : data,
        'Title' : spaCy_Doc_dict['title'],
        'Type' : 'frequency weight'
    }
    logger.info('function finished')
    return word_frequency_weight_dict

def words_context(token_dict_list_list: list[list[helpers.TokenDict | helpers.TokenPosDict]],
                  spaCy_Doc_dict: helpers.SpacyDocDict,
                  mode: Literal['paragraph', 'sentence'] = 'sentence',
                  log_dir: str ='logging/'
                  ) -> helpers.WordContextDict:
    """Returns words in context (either paragraphs or sentences).

    Args:
        token_dict_list_list (list) : List of lists of dictionaries with spacy.tokens.token.Token and optional the related POS-tags
        spaCy_Doc_dict (dict) : Dictionary containing 'spaCy_Doc' (https://spacy.io/api/doc), 'title' (str) and 'word_tokens' (int)
        mode (str) : Defines the mode ('sentence' or 'paragraph'); default: 'sentence'
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        words_context_dict (dict) : Dictionary containing 'Data' (dictionary with list of str per token), 'Title' (str), 'Type' (str)
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    data_dict = {}
    for token_dict_list in token_dict_list_list:
        lemmas_list = []
        for token_dict in token_dict_list:
            lemmas_list.append(token_dict['spaCy_Token'].lemma_)
        lemmas = '_'.join(lemmas_list)
        if mode == 'paragraph':
            logger.info('Context: paragraphs')
            contexts = context_paragraphs(token_dict_list, spaCy_Doc_dict['spaCy_Doc'])
            kind = 'context paragraph'
        elif mode == 'sentence':
            logger.info('Context: sentences')
            contexts = context_sentences(token_dict_list, spaCy_Doc_dict['spaCy_Doc'])
            kind = 'context sentence'
        data_dict.update({lemmas : contexts})
    words_context_dict = {
        'Data' : data_dict,
        'Title' : spaCy_Doc_dict['title'],
        'Type' : kind
    }
    logger.info('function finished')
    return words_context_dict

"""****************************************************
    WORD ANALYSES Module Classes
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

class Words:
    """Words Class offers methods to perform word-centered analyses (co-occurences, dependencies, contexts, and frequencies and weights).

    Args:
    corpus (helpers.CorpusDict) : Corpus including spaCy-Docs for every text-version
    nlp (spaCy Processing Pipeline) : https://spacy.io/usage/processing-pipelines
    log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Properties:
    get_tokens_from_string (list) : This property finds all spacy.tokens.token.Token which lemma (and optional POS-tag) fits to user input. It produces a list of dicitionaries with spacy.tokens.token.Token and related POS-tags.

    Class-Functions:
    corpus_context : Returns the contexts of a list of Words for each document of the corpus (context must include either one or several words); optionally: selection of the corpus; three different modes: 'paragraph', 'sentence', 'span'
    corpus_context_dataframes : Returns the results of corpus_context as DataFrames
    corpus_cooccurrences : Returns the co-occurrences and their frequencies of a list of words for each document of the corpus; optionally: selection of the corpus; four different modes: 'document', 'paragraph', 'sentence', 'span'
    corpus_cooccurrences_dataframes : Returns the results of corpus_cooccurrences as DataFrames
    corpus_cooccurrences_wordclouds : Returns the results of corpus_cooccurrences as word clouds
    corpus_dependencies : Returns all tokens which are in a certain grammatical relation to a list of words for each document of the corpus; optionally: selection of the corpus; pre-configured modes are 'subordinate' and 'superior' tokens; it is possible to use custom dependency patterns
    corpus_dependencies_dataframes : Returns the results of corpus_dependencies as DataFrames
    corpus_dependencies_wordclouds : Returns the results of corpus_dependencies as word clouds
    corpus_frequency_weight : Returns the frequency and the weight for a list of words for every document and the whole corpus; optionally: a selection
    corpus_frequency_weight_dataframes : Returns the results of corpus_frequency_weight as DataFrames
    corpus_token_instances : Returns all tokens which lemma contains the given string (for the whole corpus and for each text; optionally: selection of texts is possible)

    """
    def __init__(self, corpus: helpers.CorpusDict, nlp: spacyNLP, log_dir: str ='logging/'):
        self.log_dir = log_dir
        self.logger = helpers.logging_init(self.log_dir)
        self.logger.info('Init Frequencies.CorpusFrequencies')
        self.corpus = corpus
        self.nlp = nlp

    @property
    def get_tokens_from_string(self) -> list[helpers.TokenDict | helpers.TokenPosDict]:
        string = str(input('Bitte geben sie eine Zeichenfolge ein: '))
        input_pos = helpers.user_input_specific_POS()
        tokens_corpus = []
        for text in self.corpus['works']:
            for version in text['versions']:
                spaCy_Doc = version['spaCy_Doc']
                if input_pos == None:
                    tokens_list = tokens_from_string(spaCy_Doc, string)
                else:
                    tokens_list = tokens_from_string(spaCy_Doc, string, input_pos)
                tokens_corpus.extend(tokens_list)
        filtered_tokens_corpus = helpers.remove_spacy_duplicates(tokens_corpus)
        corpus_tokens_from_string = []
        print(Fore.GREEN + 'Liste der Lemmas, die die eingegebene Zeichenfolge enthalten:' + Style.RESET_ALL)
        for token in filtered_tokens_corpus:
            print(Fore.GREEN + 'Lemma: ' + token.lemma_ + Style.RESET_ALL)
            token_dict = {'spaCy_Token' : token}
            if input_pos != None:
                token_dict.update({'POS_tags' : input_pos})
            corpus_tokens_from_string.append(token_dict)
        return corpus_tokens_from_string

    def corpus_context(self,
                       token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict] | list[list[helpers.TokenDict | helpers.TokenPosDict]],
                       corpus_title: str,
                       title_list: None | list[helpers.TitleDict] = None,
                       mode: Literal['paragraph', 'sentence', 'span'] = 'sentence',
                       context_range: int = 5
                       ) -> helpers.WordCorpusContextDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph', 'sentence', or 'span'); default: 'sentence'
            context_range (int) : Integer for context range
        """
        self.logger.info('function started')
        context_list = []
        if title_list == None:
            self.logger.info('Frequency and weight from whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    spacy_doc_dict = {
                        'spaCy_Doc' : version['spaCy_Doc'],
                        'title' : text['metadata']['title'] + version['metadata']['title'],
                        'word_tokens' : version['quantities']['word_tokens']
                    }
                    if type(token_dict_list[0]) == list:
                        context_list.append(words_context(token_dict_list, spacy_doc_dict, mode))
                    else:
                        context_list.append(word_context(token_dict_list, spacy_doc_dict, mode, nlp=self.nlp, context_range=context_range))
        else:
            self.logger.info('Frequency and weight from selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                spacy_doc_dict = {
                                    'spaCy_Doc' : version['spaCy_Doc'],
                                    'title' : text['metadata']['title'] + version['metadata']['title'],
                                    'word_tokens' : version['quantities']['word_tokens']
                                }
                                if type(token_dict_list[0]) == list:
                                    context_list.append(words_context(token_dict_list, spacy_doc_dict, mode))
                                else:
                                    context_list.append(word_context(token_dict_list, spacy_doc_dict, mode, nlp=self.nlp, context_range=context_range))
        if type(token_dict_list[0]) == list:
            new_token_dict_list = []
            for token in context_list[0]['Data']:
                new_token_dict_list.append({'spaCy_Token' : token})
            data_dict = collect_contexts(new_token_dict_list, context_list)
        else:
            data_dict = collect_contexts(token_dict_list, context_list)
        if mode == 'paragraph':
            kind = 'context paragraph'
        elif mode == 'sentence':
            kind = 'context sentence'
        elif mode == 'span':
            kind = 'context span'
        corpus_context_dict = {
            'Data' : data_dict,
            'Title' : corpus_title,
            'Type' : kind
        }
        self.logger.info('function finished')
        return corpus_context_dict

    def corpus_context_dataframes(self,
                       token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict] | list[list[helpers.TokenDict | helpers.TokenPosDict]],
                       corpus_title: str,
                       title_list: None | list[helpers.TitleDict] = None,
                       mode: Literal['paragraph', 'sentence', 'span'] = 'sentence',
                       context_range: int = 5
                       ) -> helpers.DataFramesListDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph', 'sentence', or 'span'); default: 'sentence'
            context_range (int) : Integer for context range
        """
        self.logger.info('function started')
        context_dict = self.corpus_context(token_dict_list, corpus_title, title_list, mode, context_range)
        dataframes = []
        for token in context_dict['Data']:
            data_series = []
            for document in context_dict['Data'][token]:
                data_list = []
                for data in context_dict['Data'][token][document]:
                    data_list.append(data.text)
                data_series.append(pd.Series(data_list, name=document))
            data_frame_dict = {
                'df' : pd.concat(data_series, axis=1),
                'df_title' : token,
                'index' : False
            }
            dataframes.append(data_frame_dict)
        data_frames_list_dict = {
            'dataframes' : dataframes,
            'title' : context_dict['Title'] + ' ' + context_dict['Type']
        }
        return data_frames_list_dict

    def corpus_cooccurrences(self,
                             token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                             corpus_title: str,
                             mode: Literal['document', 'paragraph', 'sentence', 'span'] = 'sentence',
                             title_list: None | list[helpers.TitleDict] = None,
                             result_POS_tags: None | list[str] = None,
                             **spankwargs
                             ) -> helpers.WordCorpusCooccurrencesDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            **spankwargs () : Keyword arguments for mode='span' (see function context_spans)
        """
        self.logger.info('function started')
        cooccurrences_list = []
        if title_list == None:
            self.logger.info('Cooccurrences from whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    spacy_doc_dict = {
                        'spaCy_Doc' : version['spaCy_Doc'],
                        'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                        'word_tokens' : version['quantities']['word_tokens']
                    }
                    cooccurrences_list.append(word_cooccurences(token_dict_list, spacy_doc_dict, mode, result_POS_tags, nlp=self.nlp, **spankwargs))
        else:
            self.logger.info('Cooccurrences from selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                spacy_doc_dict = {
                                    'spaCy_Doc' : version['spaCy_Doc'],
                                    'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                                    'word_tokens' : version['quantities']['word_tokens']
                                }
                                cooccurrences_list.append(word_cooccurences(token_dict_list, spacy_doc_dict, mode, result_POS_tags, nlp=self.nlp, **spankwargs))
        data_dict = collect_cooccurrences_dependencies(token_dict_list, cooccurrences_list)
        if mode == 'document':
            kind =  'cooccurrences document'
        elif mode == 'paragraph':
            kind = 'cooccurrences paragraph'
        elif mode == 'sentence':
            kind = 'cooccurrences sentence'
        elif mode == 'span':
            kind = 'cooccurrences span'
        corpus_cooccurrences_dict = {
            'Data' : data_dict,
            'Title' : corpus_title,
            'Type' : kind
        }
        self.logger.info('function finished')
        return corpus_cooccurrences_dict

    def corpus_cooccurrences_dataframes(self,
                                        token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                        corpus_title: str,
                                        mode: Literal['document', 'paragraph', 'sentence', 'span'] = 'sentence',
                                        title_list: None | list[helpers.TitleDict] = None,
                                        result_POS_tags: None | list[str] = None,
                                        **spankwargs
                                        ) -> helpers.DataFramesListDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            **spankwargs () : Keyword arguments for mode='span' (see function context_spans)
        """
        self.logger.info('function started')
        cooccurrences_dict = self.corpus_cooccurrences(token_dict_list, corpus_title, mode, title_list, result_POS_tags, **spankwargs)
        data_frames_list = []
        for token in cooccurrences_dict['Data']:
            data_dict = {
                'data' : cooccurrences_dict['Data'][token],
                'title' : token
            }
            data_frame_dict = helpers.make_dataframe_dict(data_dict)
            data_frames_list.append(data_frame_dict)
        data_frames_list_dict = {
            'dataframes' : data_frames_list,
            'title' : cooccurrences_dict['Title'] + ' ' + cooccurrences_dict['Type']
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def corpus_cooccurrences_wordclouds(self,
                                        token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                        corpus_title: str,
                                        mode: Literal['document', 'paragraph', 'sentence', 'span'] = 'sentence',
                                        title_list: None | list[helpers.TitleDict] = None,
                                        result_POS_tags: None | list[str] = None,
                                        max_words: int | None = None,
                                        img_width: int = 100,
                                        img_height: int = 100,
                                        fontsize: float = 96.0,
                                        **spankwargs) -> list[helpers.FigureDict]:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
            img_width (int) : Width of the word cloud
            img_height (int) : Height of the word cloud
            fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
            **spankwargs () : Keyword arguments for mode='span' (see function context_spans)
        """
        self.logger.info('function started')
        cooccurrences_dict = self.corpus_cooccurrences(token_dict_list, corpus_title, mode, title_list, result_POS_tags, **spankwargs)
        frequencies_dict_list = []
        for token in cooccurrences_dict['Data']:
            for data in cooccurrences_dict['Data'][token]:
                if cooccurrences_dict['Data'][token][data]:
                    frequencies_dict = {
                        'data' : cooccurrences_dict['Data'][token][data],
                        'title' : cooccurrences_dict['Title'] + ' ' + token + ' ' + data
                    }
                    frequencies_dict_list.append(frequencies_dict)
        wordcloud_list = []
        print(Fore.GREEN + 'Create Wordclouds' + Style.RESET_ALL)
        for wordcloud_dict in p_imap(partial(helpers.wordcloud_pipeline, logger=self.logger, max_words=max_words, img_width=img_width, img_height=img_height, fontsize=fontsize, log_dir=self.log_dir), frequencies_dict_list):
            wordcloud_list.append(wordcloud_dict)
        self.logger.info('function finished')
        return wordcloud_list

    def corpus_dependencies(self,
                            token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                            corpus_title: str,
                            mode: Literal['custom', 'subordinate', 'superior'],
                            title_list: None | list[helpers.TitleDict] = None,
                            result_POS_tags: None | list[str] = None,
                            custom_patterns: None | list[dict] = None
                            ) -> helpers.WordCorpusDependenciesDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            custom_patterns (None | list) : None or Pattern for spaCy dependency matcher (https://spacy.io/api/dependencymatcher)
        """
        self.logger.info('function started')
        dependencies_list = []
        if title_list == None:
            self.logger.info('Dependencies from whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    spacy_doc_dict = {
                        'spaCy_Doc' : version['spaCy_Doc'],
                        'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                        'word_tokens' : version['quantities']['word_tokens']
                    }
                    dependencies_list.append(word_dependencies(token_dict_list, spacy_doc_dict, self.nlp, mode, result_POS_tags, custom_patterns))
        else:
            self.logger.info('Dependencies from selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                spacy_doc_dict = {
                                    'spaCy_Doc' : version['spaCy_Doc'],
                                    'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                                    'word_tokens' : version['quantities']['word_tokens']
                                }
                                dependencies_list.append(word_dependencies(token_dict_list, spacy_doc_dict, self.nlp, mode, result_POS_tags, custom_patterns))
        data_dict = collect_cooccurrences_dependencies(token_dict_list, dependencies_list)
        if mode == 'custom':
            kind =  'dependencies custom'
        elif mode == 'subordinate':
            kind = 'dependencies subordinate'
        elif mode == 'superior':
            kind = 'dependencies superior'
        corpus_dependencies_dict = {
            'Data' : data_dict,
            'Title' : corpus_title,
            'Type' : kind
        }
        self.logger.info('function finished')
        return corpus_dependencies_dict

    def corpus_dependencies_dataframes(self,
                                       token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                       corpus_title: str,
                                       mode: Literal['custom', 'subordinate', 'superior'],
                                       title_list: None | list[helpers.TitleDict] = None,
                                       result_POS_tags: None | list[str] = None,
                                       custom_patterns: None | list[dict] = None
                                       ) -> helpers.DataFramesListDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            custom_patterns (None | list) : None or Pattern for spaCy dependency matcher (https://spacy.io/api/dependencymatcher)
        """
        self.logger.info('function started')
        dependencies_dict = self.corpus_dependencies(token_dict_list, corpus_title, mode, title_list, result_POS_tags, custom_patterns)
        data_frames_list = []
        for token in dependencies_dict['Data']:
            data_dict = {
                'data' : dependencies_dict['Data'][token],
                'title' : token
            }
            data_frame_dict = helpers.make_dataframe_dict(data_dict)
            data_frames_list.append(data_frame_dict)
        data_frames_list_dict = {
            'dataframes' : data_frames_list,
            'title' : dependencies_dict['Title'] + ' ' + dependencies_dict['Type']
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def corpus_dependencies_wordclouds(self,
                                        token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                        corpus_title: str,
                                        mode: Literal['custom', 'subordinate', 'superior'],
                                        title_list: None | list[helpers.TitleDict] = None,
                                        result_POS_tags: None | list[str] = None,
                                        custom_patterns: None | list[dict] = None,
                                        max_words: int | None = None,
                                        img_width: int = 100,
                                        img_height: int = 100,
                                        fontsize: float = 96.0
                                        ) -> list[helpers.FigureDict]:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            mode (str) : Defines the mode ('paragraph, 'document', 'sentence', or 'span'); default: 'sentence'
            result_POS_tags (None | list) : None or list of POS-tags to specify results; default: None
            custom_patterns (None | list) : None or Pattern for spaCy dependency matcher (https://spacy.io/api/dependencymatcher)
            max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
            img_width (int) : Width of the word cloud
            img_height (int) : Height of the word cloud
            fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
        """
        self.logger.info('function started')
        dependencies_dict = self.corpus_dependencies(token_dict_list, corpus_title, mode, title_list, result_POS_tags, custom_patterns)
        frequencies_dict_list = []
        for token in dependencies_dict['Data']:
            for data in dependencies_dict['Data'][token]:
                if dependencies_dict['Data'][token][data]:
                    frequencies_dict = {
                        'data' : dependencies_dict['Data'][token][data],
                        'title' : dependencies_dict['Title'] + ' ' + token + ' ' + data
                    }
                    frequencies_dict_list.append(frequencies_dict)
        wordcloud_list = []
        print(Fore.GREEN + 'Create Wordclouds' + Style.RESET_ALL)
        for wordcloud_dict in p_imap(partial(helpers.wordcloud_pipeline, logger=self.logger, max_words=max_words, img_width=img_width, img_height=img_height, fontsize=fontsize, log_dir=self.log_dir), frequencies_dict_list):
            wordcloud_list.append(wordcloud_dict)
        self.logger.info('function finished')
        return wordcloud_list

    def corpus_frequency_weight(self,
                                token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                corpus_title: str,
                                title_list: None | list[helpers.TitleDict] = None
                                ) -> helpers.WordCorpusFrequencyWeightDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        frequency_weight_list = []
        spaCy_Doc_dict_list = []
        if title_list == None:
            self.logger.info('Frequency and weight from whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    spacy_doc_dict = {
                        'spaCy_Doc' : version['spaCy_Doc'],
                        'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                        'word_tokens' : version['quantities']['word_tokens']
                    }
                    spaCy_Doc_dict_list.append(spacy_doc_dict)
                    frequency_weight_list.append(word_frequency_weight(token_dict_list, spacy_doc_dict))
        else:
            self.logger.info('Frequency and weight from selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                spacy_doc_dict = {
                                    'spaCy_Doc' : version['spaCy_Doc'],
                                    'title' : text['metadata']['title'] + ' ' + version['metadata']['title'],
                                    'word_tokens' : version['quantities']['word_tokens']
                                }
                                spaCy_Doc_dict_list.append(spacy_doc_dict)
                                frequency_weight_list.append(word_frequency_weight(token_dict_list, spacy_doc_dict))
        data_dict = collect_frequency_weight(token_dict_list, frequency_weight_list, spaCy_Doc_dict_list)
        corpus_frequency_weight_dict = {
            'Data' : data_dict,
            'Title' : corpus_title,
            'Type' : 'frequency weight'
        }
        self.logger.info('function finished')
        return corpus_frequency_weight_dict

    def corpus_frequency_weight_dataframes(self,
                                           token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                                           corpus_title: str,
                                           title_list: None | list[helpers.TitleDict] = None
                                           ) -> helpers.DataFramesListDict:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        corpus_frequency_weight_dict = self.corpus_frequency_weight(token_dict_list, corpus_title, title_list)
        frequency_data = {}
        weight_data = {}
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            frequency_data.update({lemma : corpus_frequency_weight_dict['Data'][lemma]['frequency']})
            weight_data.update({lemma : corpus_frequency_weight_dict['Data'][lemma]['weight']})
        frequency_dict = {
            'data' : frequency_data,
            'title' : 'frequency'
        }
        frequency_df = helpers.make_dataframe_dict(frequency_dict)
        frequendy_df_transposed = {
            'df' : frequency_df['df'].T,
            'index' : frequency_df['index'],
            'df_title' : frequency_df['df_title'] + ' transposed'
        }
        weight_dict = {
            'data' : weight_data,
            'title' : 'weight'
        }
        weight_df = helpers.make_dataframe_dict(weight_dict)
        weight_df_transposed = {
            'df' : weight_df['df'].T,
            'index' : weight_df['index'],
            'df_title' : weight_df['df_title'] + ' transposed'
        }
        data_frames_list_dict = {
            'dataframes' : [frequency_df, frequendy_df_transposed, weight_df, weight_df_transposed],
            'title' : corpus_frequency_weight_dict['Title'] + ' frequency weight'
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def corpus_token_instances(self,
                               token_dict_list: list[helpers.TokenDict | helpers.TokenPosDict],
                               corpus_title: str,
                               title_list: None | list[helpers.TitleDict] = None
                               ) -> dict[str, dict[str, list[spacyToken]]]:
        """
        Args:
            token_dict_list (list) : List of dictionaries with spacy.tokens.token.Token and related POS-tags
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        instances_list = []
        if title_list == None:
            self.logger.info('Collecting instances from whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    version_title = text['metadata']['title'] + ' ' + version['metadata']['title']
                    spaCy_Doc_dict = {
                        'spaCy_Doc' : version['spaCy_Doc'],
                        'title' : version_title,
                        'word_tokens' : version['quantities']['word_tokens']
                    }
                    instances = all_token_instances(spaCy_Doc_dict, token_dict_list)
                    instances_list.append(instances)
        else:
            self.logger.info('Collecting instances from selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                version_title = text['metadata']['title'] + ' ' + version['metadata']['title']
                                spaCy_Doc_dict = {
                                    'spaCy_Doc' : version['spaCy_Doc'],
                                    'title' : version_title,
                                    'word_tokens' : version['quantities']['word_tokens']
                                }
                                instances = all_token_instances(spaCy_Doc_dict, token_dict_list)
                                instances_list.append(instances)
        all_texts = {}
        corpus_token_instances = {'Title' : corpus_title}
        for token_dict in token_dict_list:
            lemma = token_dict['spaCy_Token'].lemma_
            corpus_instances_list = []
            for instances_dict in instances_list:
                corpus_token_instances.update({instances_dict['Title'] : instances_dict['Data']})
                corpus_instances_list.extend(instances_dict['Data'][lemma])
            all_texts.update({lemma : corpus_instances_list})
        corpus_token_instances.update({'AllTexts' : all_texts})
        self.logger.info('function finished')
        return corpus_token_instances