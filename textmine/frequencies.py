# Copyright (C) 2022 Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl.html

"""
Module for frequency analyses.

Author  : Max-Ferdinand Zeterberg <zeterberg@sub.uni-goettingen.de>
Created : 2022-03-11
Version : 1.0: 2022-11-11

"""

from collections import Counter
from colorama import *
from functools import partial
import math
import matplotlib.pyplot as plt
import matplotlib.pyplot as plot # Notwendig für frequencies_bar_chart --> mit plt statt plot funktioniert es nicht
from p_tqdm import p_imap
from spacy.tokens.doc import Doc as spacyDoc
from spacy.tokens.token import Token as spacyToken

from . import helpers

"""****************************************************
    Frequencies Module Variables
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

subject_dependencies = [
        'sb',   # subject
        'sbp',  # passivized subject
]

"""****************************************************
    Frequencies Module Functions
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

def dependencies(spaCy_Doc: spacyDoc | list[spacyDoc],
                 title: str,
                 dep_label: str | list[str] = subject_dependencies,
                 lowercase: bool = False,
                 log_dir: str ='logging/'
                 ) -> helpers.DependenciesDict:
    """Counting frequencies of all instances of specific dependency relations (child to head).

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc | list) : https://spacy.io/api/doc or list of https://spacy.io/api/doc
        text_title (str) : Title of the text/corpus
        dep_label (str | list) : Dependency label as string or list of dependency labels (see vor eligible list of labels the documentation for the spacy language model in use)
        lowercase (boolean) : If true, lemmatized words will normalized
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        weights_dict (dict) : Dictionary containing 'data' (dict[str, float]) and 'title' (str; Text title + 'Keyword Weights')
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    dependency_list = []
    try:
        spaCy_Doc[0] == spacyDoc
    except:
        logger.info('Calculating Weights from one spaCyDoc')
        sentence_number = len(list(spaCy_Doc.sents))
        for token in spaCy_Doc:
            if token.dep_ in dep_label:
                dependency_list.append(token)
    else:
        logger.info('Calculating Weights from list of spaCyDocs')
        sentence_number = 0
        for s_d in spaCy_Doc:
            sentence_number += len(list(s_d.sents))
            for token in s_d:
                if token.dep_ in dep_label:
                    dependency_list.append(token)
    lemmas = helpers.lemmas_from_tokens(dependency_list, lowercase)
    dependency_frequencies = dict(Counter(lemmas))
    dependencies_dict = {
        'dependencyFrequency' : dependency_frequencies,
        'dependencyType' : dep_label,
        'sentenceNumber' : sentence_number,
        'title' : title
    }
    logger.info('function finished')
    return dependencies_dict


def display_bar_chart(dataframe_dict: helpers.DataFrameDict,
                      values_number: int = 20,
                      ascending: bool = True,
                      log_dir: str ='logging/'
                      ):
    """Showing a bar chart of the words with the highest frequencies.

    Args:
        dataframe_dict (dict) : Dictionary with the following key: 'df' (DataFrame with results of frequency analysis)
        values_number (int) : maximum number of words displayed in the bar chart
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    dataframe_dict['df'].sort_values(dataframe_dict['df'].columns[0], ascending=ascending).iloc[-values_number:].plot.barh()
    plt.show(block=True) # plot.show(block=True) ??? --> funktioniert auch mit plt.show()
    logger.info('function finished')

def frequencies(token_list: list[spacyToken],
                text_title: str,
                lowercase: bool = False,
                log_dir: str ='logging/'
                ) -> helpers.FrequenciesDict:
    """Creating a sorted Dictionary with frequencies of list items.

    Args:
        token_list (list) : List of spacy.tokens.token.Token
        text_title (str) : Title of the text
        lowercase (boolean) : If true, lemmatized words will normalized
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        frequencies_dict (dict) : Dictionary containing 'data' (dict[str, float]) and 'title' (str; Title of the list + 'Frequency Analysis')
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    lemmas = helpers.lemmas_from_tokens(token_list, lowercase)
    frequencies = dict(Counter(lemmas))
    sorted_frequencies = helpers.sort_frequencies(frequencies)
    title = text_title + ': Frequency Analysis'
    frequencies_dict = {
        'title' : title,
        'data' : sorted_frequencies
    }
    logger.info('function finished')
    return frequencies_dict

def frequencies_dict_to_df_dict(frequencies_dict: helpers.FrequenciesDict | list[helpers.FrequenciesDict],
                                title: str,
                                hasindex=True,
                                log_dir: str ='logging/',
                                **DataFrameKWARGS
                                ) -> helpers.DataFrameDict:
    """Converts a FrequencyDict into a DataFrameDict.

    Args:
        frequencies_dict (dict | list) : (List of) dictionary (dictionaries) containing 'data' (dict[str, float]) and 'title' (str)
        title (str) : Title of the DataFrameDict
        hasindex (boolean) : If index should be included or not
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory
        **DataFrameKWARGS : Keyword arguments for pandas.DataFrame (see https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html)

    Returns:
        dataframe_dict (dict) : dictionary which consist of the following keys: 'df' (DataFrame), 'df_title' (title of this DataFrame as string), and 'index' (boolean if index should be included or not)
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    try:
        frequencies_dict['data']
    except:
        logger.info('Creating DataFrame from a list of FrequenciesDicts')
        data = {}
        for freq_dict in frequencies_dict:
            data.update({freq_dict['title'] : freq_dict['data']})
    else:
        logger.info('Creating DataFrame from one FrequenciesDict')
        data = {frequencies_dict['title'] : frequencies_dict['data']}
    dataframe_data = {
        'title' : title,
        'data' : data
    }
    dataframe_dict = helpers.make_dataframe_dict(dataframe_data, hasindex=hasindex, **DataFrameKWARGS)
    logger.info('function finished')
    return dataframe_dict


def weights(spaCy_Doc: spacyDoc | list[spacyDoc],
            token_list: list[spacyToken] | list[list[spacyToken]],
            title: str,
            word_numbers: int,
            lowercase: bool = False,
            log_dir: str ='logging/'
            ) -> helpers.FrequenciesDict:
    """Calculating words' weights.

    Args:
        spaCy_Doc (spacy.tokens.doc.Doc | list) : https://spacy.io/api/doc or list of https://spacy.io/api/doc
        token_list (list) : List of spacy.tokens.token.Token or list of list of spacy.tokens.token.Token
        text_title (str) : Title of the text/corpus
        word_numbers (int) : Number of words of the text
        lowercase (boolean) : If true, lemmatized words will normalized
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Returns:
        weights_dict (dict) : Dictionary containing 'data' (dict[str, float]) and 'title' (str; Text title + 'Keyword Weights')
    """
    logger = helpers.logging_init(log_dir)
    logger.info('function started')
    try:
        spaCy_Doc[0] == spacyDoc
    except:
        logger.info('Calculating Weights from one spaCyDoc')
        sentences = list(spaCy_Doc.sents)
    else:
        logger.info('Calculating Weights from list of spaCyDocs')
        sentences = []
        for spaCyDoc in spaCy_Doc:
            sentences.extend(list(spaCyDoc.sents))
    try:
        token_list[0] == list
    except:
        logger.info('Calculating Weights from one filtered list')
        frequencies_dict = frequencies(token_list, title, lowercase)
    else:
        logger.info('Calculating Weights from list of filtered lists')
        corpus_filtered = []
        for tokens in token_list:
            corpus_filtered.extend(tokens)
        frequencies_dict = frequencies(corpus_filtered, title, lowercase)

    frequencies_data = frequencies_dict['data']
    tf_score = dict(frequencies_data)
    tf_score.update((x, y/int(word_numbers)) for x, y in tf_score.items())
    total_sentences = []
    for itm in sentences:
        total_sentences.append(itm.lemma_)
    idf_score = {}
    for itm in frequencies_data.keys():
        idf_score[itm] = sum(1 for s in total_sentences if itm in s)
    idf_score.update((x, math.log(len(sentences)/y)) for x, y in idf_score.items())
    tf_idf_score = {key: tf_score[key] * idf_score.get(key, 0) for key in tf_score.keys()}
    sorted_tf_idf_score = sorted(tf_idf_score.items(), key=lambda kv: kv[1], reverse = True)
    keywords_dict = {}
    for itm in sorted_tf_idf_score:
        keywords_dict.update({itm[0] : itm[1]})
    title = title + ': Keyword Weights'
    weights_dict = {
        'title' : title,
        'data' : keywords_dict
    }
    logger.info('function finished')
    return weights_dict

"""****************************************************
    Frequencies Classes
    ~~~~~~~~~~~~~~~~~~~~~~~~~
****************************************************"""

class CorpusFrequencies:
    """CorpusFrequencies Class offers methods to perform frequency and weight analyses.

    Args:
        corpus (helpers.FilteredCorpusDict) : Corpus updated with a filtered list of spacy.tokens.token.Token for every text-version
        lowercase (boolean) : If true, lemmatized words will normalized
        log_dir (str) : Directory to save log-file, default is the new folder 'logging' in the current working directory

    Properties:
        CorpusFrequencies.corpus (helpers.CorpusFrequenciesDict): Corpus updated with Frequencies and Weights

    Class-Functions:
        corpus_dependencies : Counting dependencies (relative numbers) of whole corpus; optionally: counting dependencies (relative numbers) of selection of documents
        corpus_part_compare : Compares the weights of a part of the corpus with the whole corpus
        corpus_part_compare_dataframes : Returns the results of corpus_part_compare to DataFrames
        corpus_part_compare_bar_charts : Diplays the results of corpus_part_compare as Bar Charts
        corpus_part_compare_wordclouds : Returns the results of corpus_part_compare as word clouds
        corpus_weights : Calculates the weights for the whole corpus (as one document)
        corpus_weights_dataframe : Returns the results of corpus_weights to DataFrames
        corpus_weights_bar_chart : Displays the results of corpus_weights as Bar Charts
        text_dependencies : Counts specific syntactical dependencies in all documents or in a selection
        text_frequencies : Collects the frequencies from all documents or from a selection
        text_frequencies_dataframes : Returns the results of text_frequencies to DataFrames (one ordered by tokens, one ordered by documents)
        text_frequencies_dataframes_bar_charts : Displays the results of text_frequencies as Bar Charts (one for each document)
        text_frequencies_wordclouds : Returns the results of text_frequencies as word clouds (one for each document)
        text_weights : Collects the weights from all documents or from a selection
        text_weights_dataframes : Returns the results of text_weights to DataFrames (one ordered by tokens, one ordered by documents)
        text_weights_dataframes_bar_charts : Displays the results of text_weights as Bar Charts (one for each document)
        text_weights_wordclouds : Returns the results of text_weights as word clouds (one for each document)

    """
    def __init__(self,
                 filteredCorpus: helpers.FilteredCorpusDict,
                 lowercase: bool = False,
                 log_dir: str ='logging/'):
        self.log_dir = log_dir
        self.logger = helpers.logging_init(self.log_dir)
        self.logger.info('Init Frequencies.CorpusFrequencies')
        self.corpus = filteredCorpus
        self.lowercase = lowercase
        self._frequencies_weights_corpus()
        self._updateMetadata()

    def _frequencies_weights_corpus(self):
        for text in self.corpus['works']:
            for version in text['versions']:
                version_title = text['metadata']['title'] + ' ' + version['metadata']['title']
                frequencies_dict = frequencies(version['filtered'], version_title, self.lowercase)
                version.update({'Frequencies' : frequencies_dict})
                weights_dict = weights(version['spaCy_Doc'], version['filtered'], version_title, version['quantities']['word_tokens'], self.lowercase)
                version.update({'Weights' : weights_dict})

    def _updateMetadata(self):
        self.corpus['metadata'].update({'type': 'FrequenciesCorpus'})

    def corpus_dependencies(self,
                            title_analysis: str,
                            title_list: None | list[helpers.TitleDict] = None,
                            dep_label: str | list[str] = subject_dependencies
                            ):
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Dependencies will only be counted for the whole corpus, no comparison with partial corpus
            dep_label (str | list) : Dependency label as string or list of dependency labels (see vor eligible list of labels the documentation for the spacy language model in use)
            lowercase (boolean) : If true, lemmatized words will normalized
        """
        whole_corpus_doc_list = []
        for text in self.corpus['works']:
            for version in text['versions']:
                whole_corpus_doc_list.append(version['spaCy_Doc'])
        whole_corpus_dependencies_dict = dependencies(whole_corpus_doc_list, 'Whole Corpus', dep_label, self.lowercase)
        rel_whole_corpus_dependency_frequencies = {}
        for lemma in whole_corpus_dependencies_dict['dependencyFrequency']:
            rel_dependency = whole_corpus_dependencies_dict['dependencyFrequency'][lemma]/whole_corpus_dependencies_dict['sentenceNumber']
            rel_whole_corpus_dependency_frequencies.update({lemma : rel_dependency})
        rel_dependencies_dict = {'Whole Corpus' : rel_whole_corpus_dependency_frequencies}
        if type(title_list) == list:
            self.logger.info('Counting dependencies for partial corpus')
            part_corpus_doc_list = []
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                part_corpus_doc_list.append(version['spaCy_Doc'])
            part_corpus_dependencies = dependencies(part_corpus_doc_list, 'Selection of Corpus', dep_label, self.lowercase)
            rel_part_corpus_dependency_frequencies = {}
            for lemma in part_corpus_dependencies['dependencyFrequency']:
                rel_dependency = part_corpus_dependencies['dependencyFrequency'][lemma]/part_corpus_dependencies['sentenceNumber']
                rel_part_corpus_dependency_frequencies.update({lemma : rel_dependency})
            rel_dependencies_dict.update({'Selection of Corpus' : rel_part_corpus_dependency_frequencies})
        rel_data_dict = {
            'data' : rel_dependencies_dict,
            'title' : title_analysis + ': Dependency frequencies (relative numbers)'
        }
        rel_data_frame_dict = helpers.make_dataframe_dict(rel_data_dict)
        self.logger.info('function finished')
        return rel_data_frame_dict

    def corpus_part_compare(self, title_list: list[helpers.TitleDict]):
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str)
        """
        self.logger.info('function started')
        whole_corpus_weights = self.corpus_weights('Whole Corpus')
        selected_works_weights = self.corpus_weights('Corpus selection', title_list)
        differences = {}
        only_corpus_weights = {}
        for itm in whole_corpus_weights['data']:
            if itm in selected_works_weights['data']:
                difference = +abs(whole_corpus_weights['data'][itm] - selected_works_weights['data'][itm])
                differences.update({itm : difference})
            else:
                only_corpus_weights.update({itm : whole_corpus_weights['data'][itm]})
        corpus_compare_part_dict = {
            'Title' : 'Keyness-Comparison',
            'FrequenciesDicts' : [
                {
                    'data' : differences,
                    'title' : 'Weights Differences'
                },
                {
                    'data' : only_corpus_weights,
                    'title' : 'Weights of tokens non-existing in corpus-selection'
                }
            ]
        }
        self.logger.info('function finished')
        return corpus_compare_part_dict

    def corpus_part_compare_dataframes(self, title_list: list[helpers.TitleDict]) -> helpers.DataFramesListDict:
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str)
        """
        self.logger.info('function started')
        corpus_compare_part_dict = self.corpus_part_compare(title_list)
        data_frames_list = []
        for frequency_dict in corpus_compare_part_dict['FrequenciesDicts']:
            dataframe_dict = frequencies_dict_to_df_dict(frequency_dict, 'Comparison: ' + frequency_dict['title'])
            data_frames_list.append(dataframe_dict)
        data_frames_list_dict = {
            'dataframes' : data_frames_list,
            'title' : 'Comparison whole corpus and partial corpus'
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def corpus_part_compare_bar_charts(self, title_list: list[helpers.TitleDict]):
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str)
        """
        self.logger.info('function started')
        data_frames_list_dict = self.corpus_part_compare_dataframes(title_list)
        for data_frame in data_frames_list_dict['dataframes']:
            if data_frame['df_title'] == 'Comparison: Weights Differences':
                display_bar_chart(data_frame, 20)
                display_bar_chart(data_frame, 20, ascending=False)
            else:
                display_bar_chart(data_frame, 20)
        self.logger.info('function started')

    def corpus_part_compare_wordclouds(self,
                                       title_list: list[helpers.TitleDict],
                                       max_words: int | None = None,
                                       percentage: float | None = None,
                                       img_width: int = 100,
                                       img_height: int = 100,
                                       fontsize: float = 96.0,
                                       ) -> list[helpers.FigureDict]:
        """
        Args:
            title_list (list) : List of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str)
            max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
            percentage (float | None) : Percentage as integer; if None default (= 0.1)
            img_width (int) : Width of the word cloud
            img_height (int) : Height of the word cloud
            fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
        """
        corpus_compare_part_dict = self.corpus_part_compare(title_list)
        wordcloud_list = []
        for wordcloud_dict in p_imap(partial(helpers.wordcloud_pipeline, logger=self.logger, max_words=max_words, percentage=percentage, img_width=img_width, img_height=img_height, fontsize=fontsize, log_dir=self.log_dir), corpus_compare_part_dict['FrequenciesDicts']):
            wordcloud_list.append(wordcloud_dict)
        return wordcloud_list

    def corpus_weights(self,
                       corpus_title: str,
                       title_list: None | list[helpers.TitleDict] = None
                       ) -> helpers.FrequenciesDict:
        """
        Args:
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        corpus_filtered = []
        spaCy_Docs_list = []
        word_number = 0
        if title_list == None:
            self.logger.info('Computing weights for whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    corpus_filtered.extend(version['filtered'])
                    spaCy_Docs_list.append(version['spaCy_Doc'])
                    word_number = word_number + version['quantities']['word_tokens']
        else:
            self.logger.info('Computing weights for selection of the corpus')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                corpus_filtered.extend(version['filtered'])
                                spaCy_Docs_list.append(version['spaCy_Doc'])
                                word_number = word_number + version['quantities']['word_tokens']
        weights_dict = weights(spaCy_Docs_list, corpus_filtered, corpus_title, word_number, self.lowercase)
        self.logger.info('function finished')
        return weights_dict

    def corpus_weights_dataframe(self,
                                 corpus_title: str,
                                 title_list: None | list[helpers.TitleDict] = None
                                 ) -> helpers.DataFrameDict:
        """
        Args:
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        weights_dict = self.corpus_weights(corpus_title, title_list)
        corpus_weights_df = frequencies_dict_to_df_dict(weights_dict, corpus_title + ' DataFrame')
        self.logger.info('function finished')
        return corpus_weights_df

    def corpus_weights_bar_chart(self,
                                 corpus_title: str, title_list: None | list[helpers.TitleDict] = None,
                                 values_number: int = 20):
        """
        Args:
            corpus_title (str) : Title of the selected texts
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        corpus_weights_dataframes = self.corpus_weights_dataframe(corpus_title, title_list)
        display_bar_chart(corpus_weights_dataframes, values_number)
        self.logger.info('function finished')

    def text_dependencies(self,
                          title: str,
                          title_list: None | list[helpers.TitleDict] = None,
                          dep_label: str | list[str] = subject_dependencies
                          ) -> helpers.DataFramesListDict:
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            dep_label (str | list) : Dependency label as string or list of dependency labels (see vor eligible list of labels the documentation for the spacy language model in use)
            lowercase (boolean) : If true, lemmatized words will normalized
        """
        self.logger.info('function started')
        abs_dependencies_dict = {}
        rel_dependencies_dict = {}
        if title_list == None:
            self.logger.info('Collecting dependencies of whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    version_title = text['metadata']['title'] + ' ' + version['metadata']['title']
                    dependencies_dict = dependencies(version['spaCy_Doc'], version_title, dep_label, self.lowercase)
                    dependency_frequencies = {version_title : dependencies_dict['dependencyFrequency']}
                    abs_dependencies_dict.update(dependency_frequencies)
                    rel_dependency_frequencies = {}
                    for lemma in dependencies_dict['dependencyFrequency']:
                        rel_dependency = dependencies_dict['dependencyFrequency'][lemma]/dependencies_dict['sentenceNumber']
                        rel_dependency_frequencies.update({lemma : rel_dependency})
                    rel_dependencies_dict.update({version_title : rel_dependency_frequencies})

        else:
            self.logger.info('Collecting dependencies of selection of texts')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                version_title = text['metadata']['title'] + ' ' + version['metadata']['title']
                                dependencies_dict = dependencies(version['spaCy_Doc'], version_title, dep_label, self.lowercase)
                                dependency_frequencies = {version_title : dependencies_dict['dependencyFrequency']}
                                abs_dependencies_dict.update(dependency_frequencies)
                                rel_dependency_frequencies = {}
                                for lemma in dependencies_dict['dependencyFrequency']:
                                    rel_dependency = dependencies_dict['dependencyFrequency'][lemma]/dependencies_dict['sentenceNumber']
                                    rel_dependency_frequencies.update({lemma : rel_dependency})
                                rel_dependencies_dict.update(rel_dependency_frequencies)
        abs_data_dict = {
            'data' : abs_dependencies_dict,
            'title' : title + ': Dependency frequencies (absolute numbers)'
        }
        rel_data_dict = {
            'data' : rel_dependencies_dict,
            'title' : title + ': Dependency frequencies (relative numbers)'
        }
        abs_data_frame_dict = helpers.make_dataframe_dict(abs_data_dict)
        abs_data_frame_dict_transposed = {
            'df' : abs_data_frame_dict['df'].T,
            'index' : abs_data_frame_dict['index'],
            'index' : abs_data_frame_dict['df_title'] + ' Transposed'
        }
        rel_data_frame_dict = helpers.make_dataframe_dict(rel_data_dict)
        rel_data_frame_dict_transposed = {
            'df' : rel_data_frame_dict['df'].T,
            'index' : rel_data_frame_dict['index'],
            'index' : rel_data_frame_dict['df_title'] + ' Transposed'
        }
        text_dependencies_dataframes = {
            'dataframes' : [abs_data_frame_dict, abs_data_frame_dict_transposed, rel_data_frame_dict, rel_data_frame_dict_transposed],
            'title' : title + ' Dependencies DataFrames'
        }
        self.logger.info('function finished')
        return text_dependencies_dataframes

    def text_frequencies(self, title_list: None | list[helpers.TitleDict] = None) -> list[helpers.FrequenciesDict]:
        """
        Args:
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        frequencies_list = []
        if title_list == None:
            self.logger.info('Collecting Frequencies of whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    frequencies_list.append(version['Frequencies'])
        else:
            self.logger.info('Collecting Frequencies of selection of texts')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                frequencies_list.append(version['Frequencies'])
        self.logger.info('function finished')
        return frequencies_list

    def text_frequencies_dataframes(self,
                                   title:str,
                                   title_list: None | list[helpers.TitleDict] = None
                                   ) -> helpers.DataFramesListDict:
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        frequencies_list = self.text_frequencies(title_list)
        frequencies_df = frequencies_dict_to_df_dict(frequencies_list, title)
        frequencies_df_transposed = {
            'df' : frequencies_df['df'].T,
            'index' : frequencies_df['index'],
            'index' : frequencies_df['df_title'] + ' Transposed'
        }
        text_frequencies_dataframes = {
            'dataframes' : [frequencies_df, frequencies_df_transposed],
            'title' : title + ' Frequency DataFrames'
        }
        self.logger.info('function finished')
        return text_frequencies_dataframes

    def text_frequencies_dataframes_bar_charts(self,
                                               title: str,
                                               title_list: None | list[helpers.TitleDict] = None
                                               ) -> helpers.DataFramesListDict:
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        frequencies_dataframes_list = []
        if title_list == None:
            self.logger.info('Collecting Frequencies of whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    frequencies_dataframes_list.append(frequencies_dict_to_df_dict(version['Frequencies'], version['Frequencies']['title']))
        else:
            self.logger.info('Collecting Frequencies of selection of works')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                frequencies_dataframes_list.append(frequencies_dict_to_df_dict(version['Frequencies'], version['Frequencies']['title']))
        for frequencies_dataframe in frequencies_dataframes_list:
            display_bar_chart(frequencies_dataframe)
        data_frames_list_dict = {
            'dataframes' : frequencies_dataframes_list,
            'title' : title + ' Frequency DataFrames per Document'
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def text_frequencies_wordclouds(self,
                                    title_list: None | list[helpers.TitleDict] = None,
                                    max_words: int | None = None,
                                    percentage: float | None = None,
                                    img_width: int = 100,
                                    img_height: int = 100,
                                    fontsize: float = 96.0,
                                    ) -> list[helpers.FigureDict]:
        """
        Args:
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
            percentage (float | None) : Percentage as integer; if None default (= 0.1)
            img_width (int) : Width of the word cloud
            img_height (int) : Height of the word cloud
            fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
        """
        self.logger.info('function started')
        frequencies_list = self.text_frequencies(title_list)
        wordcloud_list = []
        print(Fore.GREEN + 'Create Wordclouds' + Style.RESET_ALL)
        for wordcloud_dict in p_imap(partial(helpers.wordcloud_pipeline, logger=self.logger, max_words=max_words, percentage=percentage, img_width=img_width, img_height=img_height, fontsize=fontsize, log_dir=self.log_dir), frequencies_list):
            wordcloud_list.append(wordcloud_dict)
        self.logger.info('function finished')
        return wordcloud_list

    def text_weights(self, title_list: None | list[helpers.TitleDict] = None) -> list[helpers.FrequenciesDict]:
        """
        Args:
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        weights_list = []
        if title_list == None:
            self.logger.info('Collecting Weights of whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    weights_list.append(version['Weights'])
        else:
            self.logger.info('Collecting Weights of selection of texts')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                weights_list.append(version['Weights'])
        self.logger.info('function finished')
        return weights_list

    def text_weights_dataframes(self,
                                title: str,
                                title_list: None | list[helpers.TitleDict] = None
                                ) -> helpers.DataFramesListDict:
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        weights_list = self.text_weights(title_list)
        weights_df = frequencies_dict_to_df_dict(weights_list, title)
        weights_df_transposed = {
            'df' : weights_df['df'].T,
            'index' : weights_df['index'],
            'index' : weights_df['df_title'] + ' Transposed'
        }
        text_weights_dataframes = {
            'dataframes' : [weights_df, weights_df_transposed],
            'title' : title + ' Weights DataFrames'
        }
        self.logger.info('function finished')
        return text_weights_dataframes

    def text_weights_dataframes_bar_charts(self,
                                           title: str,
                                           title_list: None | list[helpers.TitleDict] = None
                                           ) -> helpers.DataFramesListDict:
        """
        Args:
            title (str) : Title for the Corpus/selected works
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
        """
        self.logger.info('function started')
        weights_dataframes_list = []
        if title_list == None:
            self.logger.info('Collecting Weights of whole corpus')
            for text in self.corpus['works']:
                for version in text['versions']:
                    weights_dataframes_list.append(frequencies_dict_to_df_dict(version['Weights'], version['Weights']['title']))
        else:
            self.logger.info('Collecting Weights of selection of works')
            for title in title_list:
                for text in self.corpus['works']:
                    if text['metadata']['title'] == title['TextTitle']:
                        for version in text['versions']:
                            if version['metadata']['title'] == title['VersionTitle']:
                                weights_dataframes_list.append(frequencies_dict_to_df_dict(version['Weights'], version['Weights']['title']))
        for weights_dataframe in weights_dataframes_list:
            display_bar_chart(weights_dataframe)
        data_frames_list_dict = {
            'dataframes' : weights_dataframes_list,
            'title' : title + ' Weights DataFrames per Document'
        }
        self.logger.info('function finished')
        return data_frames_list_dict

    def text_weights_wordclouds(self,
                                title_list: None | list[helpers.TitleDict] = None,
                                max_words: int | None = None,
                                percentage: float | None = None,
                                img_width: int = 100,
                                img_height: int = 100,
                                fontsize: float = 96.0,
                                ) -> list[helpers.FigureDict]:
        """
        Args:
            title_list (list) : None (default) or list of dictionaries containing 'TextTitle' (str) and 'VersionTitle' (str); if default: Whole corpus will be used
            max_words (int | None) : Maximum number of words displayed in the wordcloud; if None the maximum number of words will be calculated from percentage
            percentage (float | None) : Percentage as integer; if None default (= 0.1)
            img_width (int) : Width of the word cloud
            img_height (int) : Height of the word cloud
            fontsize (float) : Size of the wordcloud-title displayed above the wordcloud
        """
        self.logger.info('function started')
        weights_list = self.text_weights(title_list)
        wordcloud_list = []
        print(Fore.GREEN + 'Create Wordclouds' + Style.RESET_ALL)
        for wordcloud_dict in p_imap(partial(helpers.wordcloud_pipeline, logger=self.logger, max_words=max_words, percentage=percentage, img_width=img_width, img_height=img_height, fontsize=fontsize, log_dir=self.log_dir), weights_list):
            wordcloud_list.append(wordcloud_dict)
        self.logger.info('function finished')
        return wordcloud_list